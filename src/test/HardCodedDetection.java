package test;

import com.minhaskamal.egami.matrix.Matrix;
import com.minhaskamal.egami.matrix.transformer.RGBConverter;

public class HardCodedDetection {
	public static void main(String[] args) throws Exception {
		String workspace = System.getenv("SystemDrive") + System.getenv("HOMEPATH") + "\\Desktop\\";
		Matrix matrix = new Matrix(workspace+"in_q.jpg", Matrix.RED_GREEN_BLUE);
		
		//matrix = skinDetector2(matrix);
		matrix = redEyeDetector(matrix);
		
		matrix.write(workspace+"out_q.png");
		System.out.println("done!");
	}
	
	public static Matrix skinDetector(Matrix matrix){
		for(int i=0; i<matrix.pixels.length; i++){
			for(int j=0; j<matrix.pixels[i].length; j++){
				if(matrix.pixels[i][j][0]>95 && matrix.pixels[i][j][1]>40 && matrix.pixels[i][j][2]>20 &&
						matrix.pixels[i][j][0]>matrix.pixels[i][j][1] && matrix.pixels[i][j][0] > matrix.pixels[i][j][2] &&
						Math.abs(matrix.pixels[i][j][0]-matrix.pixels[i][j][1])>15){
					
				}else{
					matrix.pixels[i][j] = new int[]{255, 255, 255};
				}
			}
		}
		return matrix;
	}
	
	public static Matrix skinDetector2(Matrix matrix){
		
		RGBConverter rgbConverter = new RGBConverter();
		Matrix matrixHue = rgbConverter.transformToHue(matrix);
		Matrix matrixSat = rgbConverter.transformToSaturation(matrix);
		
		double hueLimMin = 255/8;
		double hueLimMax = 255-255/8;
		double satMin = 0.3*255;
		
		for(int i=0; i<matrix.pixels.length; i++){
			for(int j=0; j<matrix.pixels[i].length; j++){
				if((matrixHue.pixels[i][j][0]<hueLimMin || matrixHue.pixels[i][j][0]>hueLimMax) &&
						matrixSat.pixels[i][j][0]>satMin){
					matrix.pixels[i][j] = new int[]{255, 255, 255};
				}
			}
		}
		
		return matrix;
	}
	
	public static Matrix redEyeDetector(Matrix matrix){
		
		RGBConverter rgbConverter = new RGBConverter();
		Matrix matrixHue = rgbConverter.transformToHue(matrix);
		Matrix matrixSat = rgbConverter.transformToSaturation(matrix);
		
		double hueLimMin = 50*255/360;
		double satLimMin = 0.23*255;
		double satLimMax = 0.68*255;
		
		for(int i=0; i<matrix.pixels.length; i++){
			for(int j=0; j<matrix.pixels[i].length; j++){
				if(matrixHue.pixels[i][j][0]<hueLimMin &&
						matrixSat.pixels[i][j][0]>satLimMin && matrixSat.pixels[i][j][0]<satLimMax){
					matrix.pixels[i][j] = new int[]{255, 255, 255};
				}
			}
		}
		
		return matrix;
	}
}
