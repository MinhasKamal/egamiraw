package test;

public class ConsolePrinter {
	public static void print(int[][] args){
		for(int i=0; i<args.length; i++){
			for(int j=0; j<args[i].length; j++){
				System.out.print(args[i][j] + ", ");
			}
			System.out.println();
		}
		System.out.println();
	}
	public static void print(double[][] args){
		for(int i=0; i<args.length; i++){
			for(int j=0; j<args[i].length; j++){
				System.out.print(args[i][j] + ", ");
			}
			System.out.println();
		}
		System.out.println();
	}
	public static void print(int[] args){
		for(int i=0; i<args.length; i++){
				System.out.print(args[i] + ", ");
		}
		System.out.println();
	}
}
