package test;

import java.util.LinkedList;
import java.util.List;

import com.minhaskamal.egami.matrix.Matrix;
import com.minhaskamal.egami.matrix.enhancer.ContrastStretcher;
import com.minhaskamal.egami.matrix.enhancer.MatrixMasker;
import com.minhaskamal.egami.matrix.featureExtractor.MatrixSymbolizer;
import com.minhaskamal.egami.matrix.filters.MatrixFilter;
import com.minhaskamal.egami.matrix.utils.MatrixUtils;
import com.minhaskamal.egami.supportiveProcesses.GlobalThresholdIdentifier;

public class NoiseReducer {
	public static void main(String[] args) throws Exception {
		String workspace = System.getenv("SystemDrive") + System.getenv("HOMEPATH") + "\\Desktop\\";
		System.out.println("Started!");
		Matrix matrix = new Matrix(workspace+"in.png", Matrix.BLACK_WHITE);
		
		//enhancement
		ContrastStretcher contrastStretcher = new ContrastStretcher();
		matrix = contrastStretcher.biHistogramEqualizer(matrix);
		matrix.write(workspace+"out0.png");
		
		//noise reduction
		/*MatrixMasker matrixMasker = new MatrixMasker();
		matrix = matrixMasker.applyMask(matrix, matrixMasker.getGaussianFilter(3, 1));
		matrix.write(workspace+"out1.png");*/
		
		//quantization
		/*int[] histogram = new MatrixUtils().countPixelFreq(matrix)[0];
		
		GlobalThresholdIdentifier globalThresholdIdentifier = new GlobalThresholdIdentifier();
		int first_threshold = globalThresholdIdentifier.getOtsuThreshold(histogram);
		int[] histogram_2 = new int[256-first_threshold];
		for(int i=0; i<histogram_2.length; i++){
			histogram_2[i] = histogram[i+first_threshold];
		}
		int secondThreshold = first_threshold+globalThresholdIdentifier.getOtsuThreshold(histogram_2);
		
		List<int[]> classRangeAndValues = new LinkedList<int[]>();
		classRangeAndValues.add(new int[]{0, first_threshold, 0});
		classRangeAndValues.add(new int[]{first_threshold, secondThreshold, 200});
		classRangeAndValues.add(new int[]{secondThreshold, 256, 255});
		matrix = new MatrixSymbolizer().symbolize(matrix, classRangeAndValues, 255);
		
		matrix.write(workspace+"out2.png");*/
		
		//noise
		MatrixFilter matrixFilter = new MatrixFilter();
		matrix = matrixFilter.runMedianFilter(matrix, 7);
		matrix.write(workspace+"out3.png");
		
		System.out.println("Done!");
	}
}
