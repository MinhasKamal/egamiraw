package test;

import com.minhaskamal.egami.matrix.Matrix;
import com.minhaskamal.egami.matrixUtil.MatrixUtilities;

public class ImageTransform {
	public static void main(String[] args) throws Exception {
		String inputFilePath = "src/res/imgs/squares.png";
		String outputFolder = System.getenv("SystemDrive") + System.getenv("HOMEPATH") + "\\Desktop\\";
		
		Matrix matrix = new Matrix(inputFilePath, Matrix.RED_GREEN_BLUE_ALPHA);
		
		matrix = new MatrixUtilities().transformToLightness(matrix);
		
		matrix.write(outputFolder+"out_lit.png");
	}
}
