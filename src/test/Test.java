package test;

import com.minhaskamal.egami.matrix.Matrix;
import com.minhaskamal.egami.matrix.featureExtractor.MatrixSymbolizer;
import com.minhaskamal.egami.matrix.utils.MatrixUtils;
import com.minhaskamal.egami.supportiveProcesses.GlobalThresholdIdentifier;

public class Test {
	public static void main(String[] args) throws Exception {
		String workspace = System.getenv("SystemDrive") + System.getenv("HOMEPATH") + "\\Desktop\\";
		
		Matrix matrix = new Matrix(workspace+"in.jpg", Matrix.BLACK_WHITE);
		
		int[][] pixelFreq = MatrixUtils.countPixelFreq(matrix);
		int threshold = new GlobalThresholdIdentifier().getOtsuThreshold(pixelFreq[0]);
		matrix = new MatrixSymbolizer().convertToBinary(matrix, threshold);
		matrix.write(workspace+"outO.png");
	}
}
