package test;

import java.io.BufferedWriter;
import java.io.File;
import java.io.FileFilter;
import java.io.FileWriter;

import com.minhaskamal.egami.matrix.Matrix;
import com.minhaskamal.egami.matrix.enhancer.MatrixMasker;
import com.minhaskamal.egami.matrix.utils.MatrixUtils;

public class ImageFeatureExtractor {
	public static void main(String[] args) throws Exception {
		String workspace = System.getenv("SystemDrive") + System.getenv("HOMEPATH") + "\\Desktop\\";
		
		String data = prepareData(workspace+"data");
		writeWholeFile(workspace+"data.txt", data);
	}
	
	////////////////////////////////////////////////////////////////////////////
	
	public static String prepareData(String rootFolderPath) throws Exception{
		String[][] allFilePaths = prepareDataPaths(rootFolderPath);
		
		String data = "";
		for(int i=0; i<allFilePaths.length; i++){
			for(int j=0; j<allFilePaths[i].length; j++){
				Matrix matrix = new Matrix(allFilePaths[i][j], Matrix.BLACK_WHITE);
				//String featureString = constructFeatureString(transform(matrix));
				String featureString = constructFeatureString2(transform(matrix));
				
				data += i+featureString+"\n";
			}
			System.out.println(i);
		}
		
		return data;
	}
	
	public static String[][] prepareDataPaths(String rootFolderPath){
		File[] directories = new File(rootFolderPath).listFiles(new FileFilter() {
			@Override
			public boolean accept(File arg0) {
				return arg0.isDirectory();
			}
		});
		
		String[][] allFilePaths = new String[directories.length][];
		for(int i=0; i<directories.length; i++){
			File[] files = directories[i].listFiles();
			allFilePaths[i] = new String[files.length];
			
			for(int j=0; j<files.length; j++){
				allFilePaths[i][j] = files[j].getAbsolutePath();
			}
		}
		
		return allFilePaths;
	}
	
	////////////////////////////////////////////////////////////////////////////
	
	public static Matrix transform(Matrix matrix){
		MatrixMasker matrixMasker = new MatrixMasker();
		Matrix matrixX = matrixMasker.applyMask(matrix, matrixMasker.getSobelFilter(0));
		Matrix matrixY = matrixMasker.applyMask(matrix, matrixMasker.getSobelFilter(90));
		Matrix matrixYbyX = MatrixUtils.divideMatrixToMatrix(matrixY, matrixX);
		
		Matrix matrixTanYbyX = new Matrix(matrix.getRows(), matrix.getCols(), matrix.getType());
		
		for(int i=0; i<matrix.pixels.length; i++){
			for(int j=0; j<matrix.pixels[i].length; j++){
				matrixTanYbyX.pixels[i][j][0] = (int) Math.toDegrees(Math.atan(matrixYbyX.pixels[i][j][0]));
				matrixTanYbyX.pixels[i][j][0] += 90;
			}
		}
		
		//System.out.println(matrixTanYbyX.dump());
		
		return matrixTanYbyX;
	}
	
	///////////////////////////////////////////////////////////////////////////
	
	public static String constructFeatureString(Matrix matrix){
		String featureString = "";
		
		int[] featureVector = createFeatureVector(matrix);
		for(int i=0; i<featureVector.length; i++){
			featureString += " "+featureVector[i];
		}
		
		return featureString;
	}
	
	public static String constructFeatureString2(Matrix matrix){
		String featureString = "";
		
		int[] featureVector = createFeatureVector(matrix);
		for(int i=0; i<featureVector.length; i++){
			featureString += " "+(i+1)+":"+featureVector[i];
		}
		
		return featureString;
	}
	
	public static int[] createFeatureVector(Matrix matrix){
		
		int gridX = 8;
		int gridY = 8;
		int featureCaegory = 10;
		int featureUpperBoundary = 180;
		
		int[] featureVector = new int[gridX*gridY*featureUpperBoundary/featureCaegory];
		
		int subMatrixWidth = matrix.getCols()/gridX;
		int subMatrixHeight = matrix.getRows()/gridY;
		for(int i=0; i<gridX; i++){
			for(int j=0; j<gridY; j++){
				Matrix subMatrix = matrix.subMatrix(i*subMatrixWidth, (i+1)*subMatrixWidth,
						j*subMatrixHeight, (j+1)*subMatrixHeight);
				int[] subFeature = constructFeatureHistogram(subMatrix, featureCaegory, featureUpperBoundary);
				
				int start = (i*gridX+j)*subFeature.length;
				for(int k=0; k<subFeature.length; k++){
					featureVector[start+k] = subFeature[k];
				}
			}
		}
		
		return featureVector;
	}
	
	public static int[] constructFeatureHistogram(Matrix matrix, int featureCaegory, int featureUpperBoundary){
		int[] featureList = new int[featureUpperBoundary/featureCaegory];
		
		for(int i=0; i<matrix.pixels.length; i++){
			for(int j=0; j<matrix.pixels[i].length; j++){
				featureList[matrix.pixels[i][j][0]/featureCaegory] += matrix.pixels[i][j][0];
			}
		}
		
		return featureList;
	}
	
	/////////////////////////////////////////////////////////////////
	
	public static void writeWholeFile(String FileName, String Information) throws Exception{
		
		BufferedWriter mainBW = new BufferedWriter(new FileWriter(FileName));
		mainBW.write(Information);
		mainBW.close();
			
		return ;
	}
}
