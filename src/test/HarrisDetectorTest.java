package test;

import com.minhaskamal.egami.matrix.Matrix;
import com.minhaskamal.egami.matrix.enhancer.MatrixMasker;

public class HarrisDetectorTest {
	public static void main(String[] args) throws Exception {
		String workspace = System.getenv("SystemDrive") + System.getenv("HOMEPATH") + "\\Desktop\\";
		
		Matrix matrix = new Matrix(workspace+"in.jpg", Matrix.BLACK_WHITE);
		
		MatrixMasker matrixMasker = new MatrixMasker();
		Matrix matrixEdgeX = matrixMasker.applyMask(matrix, matrixMasker.getSobelFilter(0));
		Matrix matrixEdgeY = matrixMasker.applyMask(matrix, matrixMasker.getSobelFilter(90));
		
		///////////////////////////////////
		Matrix matrixEdgeX2 = multiplyMatrixToMatrix(matrixEdgeX, matrixEdgeX);
		Matrix matrixEdgeY2 = multiplyMatrixToMatrix(matrixEdgeY, matrixEdgeY);
		Matrix matrixEdgeXY = multiplyMatrixToMatrix(matrixEdgeX, matrixEdgeY);
		
		///////////////////////////////////
		Matrix matrixEdgeX2G = matrixMasker.applyMask(matrixEdgeX2, matrixMasker.getGaussianFilter(5, 1));
		Matrix matrixEdgeY2G = matrixMasker.applyMask(matrixEdgeY2, matrixMasker.getGaussianFilter(5, 1));
		Matrix matrixEdgeXYG = matrixMasker.applyMask(matrixEdgeXY, matrixMasker.getGaussianFilter(5, 1));
		
		///////////////////////////////////
		Matrix matrixEdgeDeterminant = substractMatrixToMatrix(multiplyMatrixToMatrix(matrixEdgeX2G, matrixEdgeY2G),
				multiplyMatrixToMatrix(matrixEdgeXYG, matrixEdgeXYG));
		
		Matrix matrixEdgeDeterminantNormalized = divideMatrixToMatrix(matrixEdgeDeterminant,
				addMatrixToMatrix(matrixEdgeX2G, matrixEdgeY2G));
		
		//System.out.println(matrixEdgeDeterminantNormalized.dump());
		
		matrixEdgeDeterminantNormalized = normalizeMatrix(matrixEdgeDeterminantNormalized);
		
		matrixEdgeDeterminantNormalized.write(workspace+"harris-corner-out.png");
		System.out.println("Done!");
	}
	
	public static Matrix multiplyMatrixToMatrix(Matrix matrix1, Matrix matrix2){
		Matrix resultMatrix = new Matrix(matrix1.getRows(), matrix1.getCols(), matrix1.getType());
		
		for(int i=0; i<resultMatrix.pixels.length; i++){
			for(int j=0; j<resultMatrix.pixels[i].length; j++){
				resultMatrix.pixels[i][j][0] = matrix1.pixels[i][j][0]*matrix2.pixels[i][j][0];
			}
		}
		
		return resultMatrix;
	}
	
	public static Matrix divideMatrixToMatrix(Matrix matrix1, Matrix matrix2){
		Matrix resultMatrix = new Matrix(matrix1.getRows(), matrix1.getCols(), matrix1.getType());
		
		for(int i=0; i<resultMatrix.pixels.length; i++){
			for(int j=0; j<resultMatrix.pixels[i].length; j++){
				if(matrix2.pixels[i][j][0]==0){
					resultMatrix.pixels[i][j][0]=Matrix.BLACK_PIXEL;
					continue;
				}
				
				resultMatrix.pixels[i][j][0] = matrix1.pixels[i][j][0]/matrix2.pixels[i][j][0];
			}
		}
		
		return resultMatrix;
	}
	
	public static Matrix addMatrixToMatrix(Matrix matrix1, Matrix matrix2){
		Matrix resultMatrix = new Matrix(matrix1.getRows(), matrix1.getCols(), matrix1.getType());
		
		for(int i=0; i<resultMatrix.pixels.length; i++){
			for(int j=0; j<resultMatrix.pixels[i].length; j++){
				resultMatrix.pixels[i][j][0] = matrix1.pixels[i][j][0]+matrix2.pixels[i][j][0];
			}
		}
		
		return resultMatrix;
	}
	
	public static Matrix substractMatrixToMatrix(Matrix matrix1, Matrix matrix2){
		Matrix resultMatrix = new Matrix(matrix1.getRows(), matrix1.getCols(), matrix1.getType());
		
		for(int i=0; i<resultMatrix.pixels.length; i++){
			for(int j=0; j<resultMatrix.pixels[i].length; j++){
				resultMatrix.pixels[i][j][0] = matrix1.pixels[i][j][0]-matrix2.pixels[i][j][0];
			}
		}
		
		return resultMatrix;
	}
	
	public static Matrix normalizeMatrix(Matrix matrix){
		for(int i=0; i<matrix.pixels.length; i++){
			for(int j=0; j<matrix.pixels[i].length; j++){
				if(matrix.pixels[i][j][0] > 30){
					matrix.pixels[i][j][0] = Matrix.WHITE_PIXEL;
				}else if(matrix.pixels[i][j][0] < 30){
					matrix.pixels[i][j][0] = Matrix.BLACK_PIXEL;
				}
			}
		}
		
		return matrix;
	}
}
