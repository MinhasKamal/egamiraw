package test;

import com.minhaskamal.egami.matrix.Matrix;
import com.minhaskamal.egami.matrix.enhancer.MorphologicalOperator;

public class MorphologicalOperatorTest {
	public static void main(String[] args) throws Exception {
		String workspace = System.getenv("SystemDrive") + System.getenv("HOMEPATH") + "\\Desktop\\";
		
		Matrix matrix = new Matrix(workspace+"in.png", Matrix.BLACK_WHITE);
		
		//matrix = MorphologicalOperator.dilate(matrix, 3, 100);
		matrix = MorphologicalOperator.erode(matrix, 3, 100);
		matrix.write(workspace+"out.png");
		
		System.out.println("Done!");
	}
}
