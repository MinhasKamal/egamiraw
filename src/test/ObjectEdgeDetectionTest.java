package test;

import com.minhaskamal.egami.matrix.Matrix;
import com.minhaskamal.egami.supportiveProcesses.ObjectEdgeDetector;

public class ObjectEdgeDetectionTest {
	public static void main(String[] args) throws Exception {
		String workspace = System.getenv("SystemDrive") + System.getenv("HOMEPATH") + "\\Desktop\\";
		
		Matrix matrix = new Matrix(workspace+"in.jpg", Matrix.BLACK_WHITE);
		
		int[] position = new ObjectEdgeDetector().leftRangeRightRange(matrix, 0, matrix.getCols()-1);
		Matrix matrix2 = matrix.subMatrix(0, matrix.pixels.length-1, position[0], position[1]);
		
		matrix2.write(workspace+"out.png");
		System.out.println("Done");
	}
}
