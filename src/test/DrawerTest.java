package test;

import java.util.ArrayList;

import com.minhaskamal.egami.matrix.Matrix;
import com.minhaskamal.egami.matrixDraw.Drawer;

public class DrawerTest {
	public static void main(String[] args) throws Exception {

		Matrix matrix = new Matrix(50, 50, Matrix.BLACK_WHITE);
		
		Drawer drawer = new Drawer();
		
		ArrayList<int[]> positions = new ArrayList<int[]>();
		positions.add(new int[]{5, 20});
		positions.add(new int[]{5, 30});
		positions.add(new int[]{35, 25});
		matrix = drawer.drawPoint(matrix, positions, 5, 5, new int[]{255});
		
		matrix = drawer.drawLine(matrix, new int[]{20, 23, 25, 27}, 0, 5, 45, new int[]{255});
		matrix = drawer.drawGrid(matrix, 5, 5, 10, 5, 20, 30, 2, 2, new int[]{255});
		
		matrix.write(System.getenv("SystemDrive") + System.getenv("HOMEPATH") + "\\Desktop\\" + "grid.png");
	}
}
