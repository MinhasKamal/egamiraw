package test;

import com.minhaskamal.egami.matrix.Matrix;
import com.minhaskamal.egami.matrix.filters.MatrixSegmentizer;

public class ImageTest {
	public static void main(String[] args) throws Exception {
		String workspace = System.getenv("SystemDrive") + System.getenv("HOMEPATH") + "\\Desktop\\";
		
		Matrix matrix = new Matrix(workspace+"in.png", Matrix.BLACK_WHITE);
		
		MatrixSegmentizer matrixSegmentizer = new MatrixSegmentizer();
		int[] histogram = matrixSegmentizer.verticalHistogram(matrix, 100);
		Matrix[] subMatrixes = matrixSegmentizer.verticallySegmentize(matrix, histogram, 5);
		System.out.println(subMatrixes.length);
		for(int i=0; i<subMatrixes.length; i++){
			subMatrixes[i].write(workspace+i+"img.png");
		}
	}
}
