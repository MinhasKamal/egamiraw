package test;

import java.io.IOException;
import java.util.ArrayList;

import com.minhaskamal.egami.matrix.Matrix;
import com.minhaskamal.egami.matrix.featureExtractor.HoughTransformer;
import com.minhaskamal.egami.matrixDraw.Drawer;

public class HoughTransformTest {
	public static void main(String[] args) throws IOException {
		String workspace = System.getenv("SystemDrive") + System.getenv("HOMEPATH") + "\\Desktop\\";
		
		Matrix matrix = new Matrix(workspace+"in2.png", Matrix.RED_GREEN_BLUE);
		
		HoughTransformer houghTransformer = new HoughTransformer(85, 95);
		ArrayList<int[]> lines = houghTransformer.applyHoughTransformer(matrix, 10, 40);
		
		//Matrix matrix2 = new Matrix(matrix.getRows(), matrix.getCols(), Matrix.BLACK_WHITE);
		
		Drawer drawer = new Drawer();
		for(int i=0; i<lines.size(); i++){
			drawer.drawLine(matrix, lines.get(i)[1], lines.get(i)[2], new int[]{255, 0, 0});
		}
		
		matrix.write(workspace+"hough-lines-f.png");
		System.out.println("Done!");
	}
}
