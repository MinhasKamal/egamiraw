package test;

import com.minhaskamal.egami.matrix.Matrix;
import com.minhaskamal.egami.matrix.utils.MatrixUtils;
import com.minhaskamal.egami.supportiveProcesses.Clustering;

public class ImageSegmentationTest {
	//test
	public static void main(String[] args) throws Exception {
		String workspace = System.getenv("SystemDrive") + System.getenv("HOMEPATH") + "\\Desktop\\";
		String inputImage = workspace+"in.jpg";
		
		Matrix matrix = new Matrix(inputImage, Matrix.RED_GREEN_BLUE);
		int[][] dataPoints = MatrixUtils.vectorize(matrix);
		
		Clustering kMeansCluster = new Clustering();
		int[][] clusterMeans = kMeansCluster.kMeansClustering(dataPoints, 2);
		//int[][] clusterMeans = kMeansCluster.meanShiftClustering(dataPoints, 10, 20);
		int[] pointInClusters = kMeansCluster.assignPoitToCluster(dataPoints, clusterMeans);
		ConsolePrinter.print(clusterMeans);
		
		for(int i=0; i<dataPoints.length; i++){
			for(int j=0; j<dataPoints[0].length; j++){
				dataPoints[i] = clusterMeans[pointInClusters[i]].clone();
			}
		}
		
		matrix = MatrixUtils.createMatrix(dataPoints, matrix.getRows(), matrix.getCols());
		matrix.write(inputImage+"-out222.png");
		
		System.out.println("Done");
	}
}
