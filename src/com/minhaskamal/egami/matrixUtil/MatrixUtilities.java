/***********************************************************
* Developer: Minhas Kamal(minhaskamal024@gmail.com)        *
* Date: 14-Dec-2014                                        *
* Modification Date: 10-Mar-2015                           *
* Modification Date: 13-Dec-2015                           *
* Modification Date: 30-Mar-2016                           *
***********************************************************/

package com.minhaskamal.egami.matrixUtil;

import com.minhaskamal.egami.matrix.Matrix;
import com.minhaskamal.egami.matrix.converter.MatrixTypeConverter;
import com.minhaskamal.egami.matrix.editor.MatrixBorderCreator;
import com.minhaskamal.egami.matrix.editor.MatrixCropper;
import com.minhaskamal.egami.matrix.editor.MatrixResizer;
import com.minhaskamal.egami.matrix.editor.MatrixRotator;
import com.minhaskamal.egami.matrix.enhancer.ContrastStretcher;
import com.minhaskamal.egami.matrix.enhancer.MatrixMasker;
import com.minhaskamal.egami.matrix.enhancer.MatrixQuantizer;
import com.minhaskamal.egami.matrix.enhancer.SpatialBrightnessEqualizer;
import com.minhaskamal.egami.matrix.featureExtractor.EdgeDetector;
import com.minhaskamal.egami.matrix.featureExtractor.LocalBinaryPatternDetector;
import com.minhaskamal.egami.matrix.featureExtractor.MatrixSymbolizer;
import com.minhaskamal.egami.matrix.featureExtractor.PixelPooler;
import com.minhaskamal.egami.matrix.filters.MatrixPixelGroupExtractor;
import com.minhaskamal.egami.matrix.transformer.RGBConverter;

public class MatrixUtilities {

	public Matrix transform(Matrix matrix, double multiplierOfRed, double multiplierOfGreen,
			double multiplierOfBlue, int adjustment){
		
		return new RGBConverter().transform(matrix, multiplierOfRed, multiplierOfGreen, multiplierOfBlue, adjustment);
	}
	
	public Matrix transformToY(Matrix matrix){
		
		return new RGBConverter().transformToY(matrix);
	}
	public Matrix transformToCb(Matrix matrix){
		
		return new RGBConverter().transformToCb(matrix);
	}
	public Matrix transformToCr(Matrix matrix){

		return new RGBConverter().transformToCr(matrix);
	}
	
	public Matrix transformToHue(Matrix matrix){

		return new RGBConverter().transformToHue(matrix);
	}
	
	public Matrix transformToSaturation(Matrix matrix){

		return new RGBConverter().transformToSaturation(matrix);
	}
	
	public Matrix transformToValue(Matrix matrix){

		return new RGBConverter().transformToValue(matrix);
	}
	
	public Matrix transformToLightness(Matrix matrix){

		return new RGBConverter().transformToLightness(matrix);
	}

	public Matrix convertType(Matrix matrix, int convertionType){
		
		return new MatrixTypeConverter().convert(matrix, convertionType);
	}
	
	public Matrix convertToBinary(Matrix matrix, int threshold){
		
		return new MatrixSymbolizer().convertToBinary(matrix, threshold);
	}
	
	public Matrix rotateLeft(Matrix matrix){
		
		return new MatrixRotator().rotateLeft(matrix);
	}
	
	public Matrix rotateRight(Matrix matrix){

		return new MatrixRotator().rotateRight(matrix);
	}
	
	public Matrix flipVertical(Matrix matrix){

		return new MatrixRotator().flipVertical(matrix);
	}
	
	public Matrix flipHorizontal(Matrix matrix){

		return new MatrixRotator().flipHorizontal(matrix);
	}

	public Matrix skewHorizontal(Matrix matrix, double angle){

		return new MatrixRotator().skewHorizontal(matrix, angle);
	}
	
	public Matrix skewVertical(Matrix matrix, double angle){

		return new MatrixRotator().skewVertical(matrix, angle);
	}
	
	public Matrix crop(Matrix matrix, int left, int right, int top, int down){
		return new MatrixCropper().crop(matrix, top, right, down, left);
	}
	
	public Matrix createBorder(Matrix matrix, int breadth){	
		
		return new MatrixBorderCreator().createBorder(matrix, breadth);
	}
	
	public Matrix createBorder(Matrix matrix, int breadth, int pixelValue){

		return new MatrixBorderCreator().createBorder(matrix, breadth, pixelValue);
	}
	
	public Matrix createBorder(Matrix matrix, int horizontalBreadth, int verticalBreadth, int borderPixel[]){

		return new MatrixBorderCreator().createBorder(matrix, horizontalBreadth, verticalBreadth, borderPixel);
	}
	
	public Matrix rotate(Matrix matrix, double angle){

		return new MatrixRotator().rotate(matrix, angle);
	}
	
	public Matrix rotate2(Matrix matrix, double angle){
		
		return new MatrixRotator().rotate2(matrix, angle);
	}
	
	public Matrix applyMask(Matrix matrix, double[][] mask) {

		return new MatrixMasker().applyMask(matrix, mask);
	}
	
	public Matrix horizontallyStretch(Matrix matrix, double percentage){
		
		return new MatrixResizer().horizontallyStretch(matrix, percentage);
	}
	
	public Matrix verticallyStretch(Matrix matrix, double percentage){

		return new MatrixResizer().verticallyStretch(matrix, percentage);
	}
	
	public Matrix resize(Matrix matrix, double horizontalPercentage, double verticalPercentage){
		
		return new MatrixResizer().resize(matrix, horizontalPercentage, verticalPercentage);
	}
	
	public Matrix resize(Matrix matrix, double percentage){
		
		return new MatrixResizer().resize(matrix, percentage);
	}
	
	public Matrix resize(Matrix matrix, int length, int width){
		
		return new MatrixResizer().resize(matrix, length, width);
	}
	
	public Matrix applyQuantization(Matrix mat, int numberOfColors){
		
		return new MatrixQuantizer().applyQuantization(mat, numberOfColors);
	}
	
	public Matrix applyEdgeDetecton(Matrix mat){
		
		return new EdgeDetector().applyEdgeDetecton(mat);
	}
	
	public Matrix applyLocalBinaryPattern(Matrix mat){
		
		return new LocalBinaryPatternDetector().extractLocalBinaryPattern(mat);
	}
	
	public Matrix spatialBrightnessEqualizer(Matrix mat, int spatialRange){
		
		return new SpatialBrightnessEqualizer().equalize(mat, spatialRange);
	}

	public Matrix histogramEqualizer(Matrix mat){

		return new ContrastStretcher().histogramEqualizer(mat);
	}
	
	public Matrix biHistogramEqualizer(Matrix mat) {

		return new ContrastStretcher().biHistogramEqualizer(mat);
	}
	
	public Matrix spatialBiHistogramEqualizer(Matrix mat, int sectionWidth, int sectionHeight) {
		
		return new ContrastStretcher().spatialBiHistogramEqualizer(mat, sectionWidth, sectionHeight);
	}
	
	public Matrix applyBlurring(Matrix mat, int level){
		
		return new MatrixMasker().applyBlurring(mat, level);
	}
	
	public Matrix applyVerticalEdgeDetection(Matrix mat){
		
		return new MatrixMasker().applyVerticalEdgeDetection(mat);
	}
	
	public Matrix applyHorizontalEdgeDetection(Matrix mat){
		
		return new MatrixMasker().applyHorizontalEdgeDetection(mat);
	}

	public Matrix getPooledFeature(Matrix matrix, int regionWidth, int regionHeight) {
		
		return new PixelPooler().applyMinPooling(matrix, regionWidth, regionHeight);
	}

	public Matrix extractPixelGroup(Matrix matrix, int threshold,
			int minNumberOfPixels, int maxNumberOfPixels){
		
		return extractPixelGroup(matrix, threshold, 0, 0, (int)Math.pow(2, 30), (int)Math.pow(2, 30), minNumberOfPixels, maxNumberOfPixels, 0, 100);
	}
	
	public Matrix extractPixelGroup(Matrix matrix, int threshold,
			int minWidth, int minHeight, int maxWidth, int maxHeight){
		
		return extractPixelGroup(matrix, threshold, minWidth, minHeight, maxWidth, maxHeight, 0, (int)Math.pow(2, 30), 0, 100);
	}
	
	public Matrix extractPixelGroup(Matrix matrix, int threshold,
			int minWidth, int minHeight, int maxWidth, int maxHeight,
			int minNumberOfPixels, int maxNumberOfPixels, int minPixelPercentage, int maxPixelPercentage){
		
		MatrixPixelGroupExtractor matrixPixelGroupExtractor = new MatrixPixelGroupExtractor(matrix);
		matrixPixelGroupExtractor.extractPixelGroup(threshold, minWidth, minHeight, maxWidth, maxHeight,
				minNumberOfPixels, maxNumberOfPixels, minPixelPercentage, maxPixelPercentage);
		
		return matrixPixelGroupExtractor.getFilteredMatrix();
	}

	///test/////////////////////////////////////////////////////////////////////////////////////////////
	public static void main(String[] args) {
		try {
			//String inputImageFilePath = "src/demo/res/imgs/color_triangle.png";
			String inputImageFilePath = System.getenv("SystemDrive") + System.getenv("HOMEPATH") + "\\Desktop\\3.PNG";
			String outputImageFilePath = System.getenv("SystemDrive") + System.getenv("HOMEPATH") + "\\Desktop\\out3.png";
	
			Matrix matrix = new Matrix(inputImageFilePath, Matrix.RED_GREEN_BLUE);
			MatrixUtilities matrixUtilities = new MatrixUtilities();
			
//			new RGBHistogram().generateRGBHistogram(matrix, 2, 200).write(inputImageFilePath+"_hist1.png");
//			matrix = matrixUtilities.biHistogramEqualizer(matrix);
//			matrix = matrixUtilities.applyBlurring(matrix, 1);
//			
//			int[] histogram = new MatrixUtils().countPixelFreq(matrix)[0];
//			int threshold = new GlobalThresholdIdentifier().getOtsuThreshold(histogram);
//			int c=5;
//			while(c --> 0){
//				int[] histogram_2 = new int[256-threshold];
//				for(int i=0; i<histogram_2.length; i++){
//					histogram_2[i] = histogram[i+threshold];
//				}
//				threshold += new GlobalThresholdIdentifier().getOtsuThreshold(histogram_2);
//				System.out.println("threshold " + threshold);
//			}
//			
//			System.out.println("threshold " + threshold);
//			new RGBHistogram().generateRGBHistogram(matrix, 2, 200).write(inputImageFilePath+"_hist2.png");
			
			matrix = matrixUtilities.convertToBinary(matrix, 140);
			
//			
//			matrix = horizontalResizing(matrix, 814);
//			matrix = verticalResizing(matrix, 814);
//
//			for(int i=600; i<1300; i+=3){
//				resize(matrix, i);
//				System.out.println(i);
//			}
//
//			matrix = resize(matrix, 30);
//
//			MatrixUtil.write(matrix, "C:\\Users\\admin\\Desktop\\d.png");
//
//			matrix = convertTo(matrix, Matrix.RED_GREEN_BLUE);
//
//			double[][] mask = {
//					{1.0/25, 1.0/25, 1.0/25, 1.0/25, 1.0/25},
//					{1.0/25, 1.0/25, 1.0/25, 1.0/25, 1.0/25},
//					{1.0/25, 1.0/25, 1.0/25, 1.0/25, 1.0/25},
//					{1.0/25, 1.0/25, 1.0/25, 1.0/25, 1.0/25},
//					{1.0/25, 1.0/25, 1.0/25, 1.0/25, 1.0/25}
//			};
//
//			double[][] mask = {
//					{1.0/9, 1.0/9, 1.0/9},
//					{1.0/9, 1.0/9, 1.0/9},
//					{1.0/9, 1.0/9, 1.0/9}
//			};
//
//			double[][] mask = {
//					{4.0/33, 4.0/33, 4.0/33},
//					{4.0/33, 1.0/18, 4.0/33},
//					{4.0/33, 4.0/33, 4.0/33}
//			};
//
//			matrix = masking(matrix, mask);
//			matrix = border(matrix, 5, new int[]{0,0,0,255});
//
//			matrix = flipVertical(matrix);
//			matrix = crop(matrix, 5, 2, 10, 15);
//			matrix = rotateLeft(matrix);
//			matrix = rotateRight(matrix);
//	
//			mat = spatialBrightnessEqualizer(mat, 4);
//			mat = convertToEdgePixels(mat, 30);
//			mat = applyVerticalEdgeDetection(mat);
//			mat = spatialBiHistogramEqualizer(mat, 100, 100);
//			mat = histogramEqualizer(mat);
//			mat = convertToLocalBinaryPattern(mat);
//			mat = applyBlurring(mat, 3);

			
			matrix.write(outputImageFilePath);
			
			System.out.println("OPERATION SUCCESSFUL!!!");
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
}
