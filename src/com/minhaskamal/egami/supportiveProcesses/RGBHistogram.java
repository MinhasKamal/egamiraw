/***********************************************************
* Developer: Minhas Kamal(minhaskamal024@gmail.com)        *
* Date: 28-Jan-2015                                        *
* Modification Date: 01-Jan-2016                           *
***********************************************************/

package com.minhaskamal.egami.supportiveProcesses;

import java.io.IOException;

import com.minhaskamal.egami.matrix.Matrix;
import com.minhaskamal.egami.matrix.utils.MatrixUtils;
import com.minhaskamal.egami.matrixUtil.MatrixUtilities;

public class RGBHistogram {
	
	public RGBHistogram() {
		
	}
	
	public Matrix generateRGBHistogram(Matrix matrix, int barWidth, int histHeight){
		int colorRange = Matrix.WHITE_PIXEL-Matrix.BLACK_PIXEL+1;
		
		if(matrix.getType()!=Matrix.RED_GREEN_BLUE){
			matrix = new MatrixUtilities().convertType(matrix, Matrix.RED_GREEN_BLUE);
		}
		
		int verticalBuffer = barWidth;
		int horizontalBuffer = barWidth;
		
		int[][] rGBFreq = MatrixUtils.countPixelFreq(matrix);
		int highestFreq = getHighestFreq(rGBFreq);

		double multiplier = (histHeight-1)/(double)highestFreq;
		
		Matrix matrixHistogram = new Matrix(histHeight+verticalBuffer, colorRange*barWidth + 2*horizontalBuffer,
				Matrix.RED_GREEN_BLUE);
		int[] rGBDot = new int[3];
		int[] initDot = {60, 60, 60};

		for (int i=0, j; i<colorRange; i++) {
			for(j=0; j<histHeight; j++){
				for(int m=0; m<barWidth; m++){
					matrixHistogram.setPixel(j, i*barWidth + m + horizontalBuffer, initDot);
				}
			}
			
			for(int k=0; k<3; k++){
				j = (int) (histHeight-( rGBFreq[k][i]*multiplier ));
				
				for ( ; j<histHeight; j++) {
					rGBDot = matrixHistogram.getPixel(j, i*barWidth + horizontalBuffer);
					rGBDot[k] = 250;
					
					for(int m=0; m<barWidth; m++){
						matrixHistogram.setPixel(j, i*barWidth + m + horizontalBuffer, rGBDot);
					}
				}
			}
		}
		
		return matrixHistogram;
	}
	
	private int getHighestFreq(int[][] rGBFreq) {
		int highestFreq=0;
		
		for(int i=0; i<3; i++){
			for(int j=0; j<256; j++){
				if(highestFreq<rGBFreq[i][j]){
					highestFreq=rGBFreq[i][j];
				}
			}
		}
		
		return highestFreq;
	}
	
	///test only
	public static void main(String[] args) {
		try {
			Matrix matrix = new Matrix("C:\\Users\\admin\\Desktop\\real.png", Matrix.RED_GREEN_BLUE_ALPHA);
			
			Matrix matrixHistogram = new RGBHistogram().generateRGBHistogram(matrix, 3, 350);
			matrixHistogram.write("C:\\Users\\admin\\Desktop\\hist7.png");
			
			System.out.println("Operation Successful!!");
		} catch (IOException e) {
			e.printStackTrace();
		}
	}
}
