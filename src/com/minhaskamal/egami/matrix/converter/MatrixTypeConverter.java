/************************************************************************************************************
* Developer: Minhas Kamal(minhaskamal024@gmail.com)															*
* Date: Dec-2015																							*
************************************************************************************************************/

package com.minhaskamal.egami.matrix.converter;

import com.minhaskamal.egami.matrix.Matrix;

public class MatrixTypeConverter {
	
	public MatrixTypeConverter() {
		
	}
	
	/**
	 * 
	 * @param convertionType The type of output matrix. If the type matches same <code>Matrix</code> 
	 * object is returned.
	 * @return
	 */
	public Matrix convert(Matrix matrix, int convertionType){
		int row = matrix.getRows();
		int col = matrix.getCols();
		int type = matrix.getType();
		
		Matrix matrix2 = new Matrix(row, col, convertionType);
		
		
		if(type==Matrix.BLACK_WHITE){
			
			if(convertionType==Matrix.BLACK_WHITE){
				matrix2 = matrix;
				
			}else if(convertionType==Matrix.BLACK_WHITE_ALPHA){
				for(int i=0, j; i<row; i++){
					for(j=0; j<col; j++){
						matrix2.pixels[i][j][0] = matrix.pixels[i][j][0];
						matrix2.pixels[i][j][1] = 255;
					}
				}
				
			}else if(convertionType==Matrix.RED_GREEN_BLUE){
				for(int i=0, j; i<row; i++){
					for(j=0; j<col; j++){
						matrix2.pixels[i][j][0] = matrix.pixels[i][j][0];
						matrix2.pixels[i][j][1] = matrix.pixels[i][j][0];
						matrix2.pixels[i][j][2] = matrix.pixels[i][j][0];
					}
				}
				
			}else{
				for(int i=0, j; i<row; i++){
					for(j=0; j<col; j++){
						matrix2.pixels[i][j][0] = matrix.pixels[i][j][0];
						matrix2.pixels[i][j][1] = matrix.pixels[i][j][0];
						matrix2.pixels[i][j][2] = matrix.pixels[i][j][0];
						matrix2.pixels[i][j][3] = 255;
					}
				}
				
			}
			
			
		}else if(type==Matrix.BLACK_WHITE_ALPHA){
			
			if(convertionType==Matrix.BLACK_WHITE){
				for(int i=0, j; i<row; i++){
					for(j=0; j<col; j++){
						matrix2.pixels[i][j][0] = matrix.pixels[i][j][0];
					}
				}
				
			}else if(convertionType==Matrix.BLACK_WHITE_ALPHA){
				matrix2 = matrix;
				
			}else if(convertionType==Matrix.RED_GREEN_BLUE){
				for(int i=0, j; i<row; i++){
					for(j=0; j<col; j++){
						matrix2.pixels[i][j][0] = matrix.pixels[i][j][0];
						matrix2.pixels[i][j][1] = matrix.pixels[i][j][0];
						matrix2.pixels[i][j][2] = matrix.pixels[i][j][0];
					}
				}
				
			}else{
				for(int i=0, j; i<row; i++){
					for(j=0; j<col; j++){
						matrix2.pixels[i][j][0] = matrix.pixels[i][j][0];
						matrix2.pixels[i][j][1] = matrix.pixels[i][j][0];
						matrix2.pixels[i][j][2] = matrix.pixels[i][j][0];
						matrix2.pixels[i][j][3] = matrix.pixels[i][j][1];
					}
				}
				
			}
			
		}else if(type==Matrix.RED_GREEN_BLUE){
			
			if(convertionType==Matrix.BLACK_WHITE){
				for(int i=0, j; i<row; i++){
					for(j=0; j<col; j++){
						matrix2.pixels[i][j][0] = (int) (matrix.pixels[i][j][0] * 0.3 +
														matrix.pixels[i][j][1] * 0.59 +
														matrix.pixels[i][j][2] * 0.11);
					}
				}
				
			}else if(convertionType==Matrix.BLACK_WHITE_ALPHA){
				for(int i=0, j; i<row; i++){
					for(j=0; j<col; j++){
						matrix2.pixels[i][j][0] = (int) (matrix.pixels[i][j][0] * 0.3 +
														matrix.pixels[i][j][1] * 0.59 +
														matrix.pixels[i][j][2] * 0.11);
						matrix2.pixels[i][j][1] = 255;
					}
				}
				
			}else if(convertionType==Matrix.RED_GREEN_BLUE){
				matrix2 = matrix;
				
			}else{
				for(int i=0, j; i<row; i++){
					for(j=0; j<col; j++){
						matrix2.pixels[i][j][0] = matrix.pixels[i][j][0];
						matrix2.pixels[i][j][1] = matrix.pixels[i][j][1];
						matrix2.pixels[i][j][2] = matrix.pixels[i][j][2];
						matrix2.pixels[i][j][3] = 255;
					}
				}
				
			}
			
		}else{
			if(convertionType==Matrix.BLACK_WHITE){
				for(int i=0, j; i<row; i++){
					for(j=0; j<col; j++){
						matrix2.pixels[i][j][0] = (int) (matrix.pixels[i][j][0] * 0.3 +
														matrix.pixels[i][j][1] * 0.59 +
														matrix.pixels[i][j][2] * 0.11);
					}
				}
				
			}else if(convertionType==Matrix.BLACK_WHITE_ALPHA){
				for(int i=0, j; i<row; i++){
					for(j=0; j<col; j++){
						matrix2.pixels[i][j][0] = (int) (matrix.pixels[i][j][0] * 0.3 +
														matrix.pixels[i][j][1] * 0.59 +
														matrix.pixels[i][j][2] * 0.11);
						matrix2.pixels[i][j][1] = matrix.pixels[i][j][3];
					}
				}
				
			}else if(convertionType==Matrix.RED_GREEN_BLUE){
				for(int i=0, j; i<row; i++){
					for(j=0; j<col; j++){
						matrix2.pixels[i][j][0] = matrix.pixels[i][j][0];
						matrix2.pixels[i][j][1] = matrix.pixels[i][j][1];
						matrix2.pixels[i][j][2] = matrix.pixels[i][j][2];
					}
				}
				
			}else{
				matrix2 = matrix;
			}
			
		}
		
		return matrix2;
	}
}
