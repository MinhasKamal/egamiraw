/************************************************************************************************************
* Developer: Minhas Kamal(minhaskamal024@gmail.com)															*
* Date: Dec-2015																							*
************************************************************************************************************/

package com.minhaskamal.egami.matrix.utils;

import java.util.ArrayList;

import com.minhaskamal.egami.matrix.Matrix;

public class MatrixUtils {
	
	/**
	 * Returns number of occurrences of all the pixels in a image.
	 * @param matrix
	 * @return
	 */
	public static int[][] countPixelFreq(Matrix matrix){
		int rows = matrix.getRows(),
			cols = matrix.getCols(),
			type = matrix.getType();
		
		int[][] pixelFreq = new int[matrix.getType()][Matrix.BLACK_PIXEL+1];
		for(int i=0; i<rows; i++) {
			for(int j=0; j<cols; j++) {
				for(int k=0; k<type; k++){
					pixelFreq[k][matrix.pixels[i][j][k]]++;
				}
			}
		}
		
		return pixelFreq;
	}
	
	/**
	 * Returns the average of all pixels of an image.
	 * @param mat matrix can be of type
	 * @return
	 */
	public static double getMeanPixelValue(Matrix matrix){
		int rows = matrix.getRows(),
			cols = matrix.getCols(),
			type = matrix.getType();
		
		//find the ration//
		double sumOfPixelByRow = 0;
		double sumOfPixelByColInRow = 0;
		for(int x=0, y, z; x<rows; x++){
			sumOfPixelByRow=0;
			for(y=0; y<cols; y++){
				for(z=0; z<type; z++){
					sumOfPixelByRow += matrix.pixels[x][y][z];
				}
			}
			
			sumOfPixelByColInRow += ((sumOfPixelByRow/3)/cols);
		}
		
		return sumOfPixelByColInRow/rows;
	}
	
	/**
	 * Creates grey scale Matrix.
	 * @param connectedDots
	 * @return
	 */
	public static Matrix createMatrixFromPoints(ArrayList<int[]> connectedDots){
		
		int[] minXMaxXMinYMaxY = getMinXMaxXMinYMaxY(connectedDots);

		int row = minXMaxXMinYMaxY[3]-minXMaxXMinYMaxY[2]+1,
			col = minXMaxXMinYMaxY[1]-minXMaxXMinYMaxY[0]+1;

		Matrix matrix = new Matrix(row, col, Matrix.BLACK_WHITE);
		for(int i=0; i<row; i++){
			for(int j=0; j<col; j++){
				matrix.pixels[i][j][0] = Matrix.WHITE_PIXEL;
			}
		}

		for(int i=0; i<connectedDots.size(); i++){
			matrix.pixels[connectedDots.get(i)[0]-minXMaxXMinYMaxY[0]][connectedDots.get(i)[1]-minXMaxXMinYMaxY[2]][0] = Matrix.BLACK_PIXEL;
		}

		return matrix;
	}
	
	/**
	 * Calculates boundary of connected dots.
	 * @param connectedDots
	 * @return
	 */
	public static int[] getMinXMaxXMinYMaxY(ArrayList<int[]> connectedDots){
		int minX=connectedDots.get(0)[0],
			maxX=connectedDots.get(0)[0],
			minY=connectedDots.get(0)[1],
			maxY=connectedDots.get(0)[1];
		
		for(int i=1; i<connectedDots.size(); i++){
			if(connectedDots.get(i)[0] < minX){
				minX = connectedDots.get(i)[0];
			}else if(connectedDots.get(i)[0] > maxX){
				maxX = connectedDots.get(i)[0];
			}

			if(connectedDots.get(i)[1] < minY){
				minY = connectedDots.get(i)[1];
			}else if(connectedDots.get(i)[1] > maxY){
				maxY = connectedDots.get(i)[1];
			}
		}
		
		return new int[]{minX, maxX, minY, maxY};
	}
	
	//////////////////////////////////////////////////////////////////////
	
	/**
	 * Returns a pixel of Matrix carrying the pixelValue.
	 * @param matrix
	 * @param pixelValue
	 * @return
	 */
	public static int[] getCustomPixel(Matrix matrix, int pixelValue){
		int[] customPixel = new int[matrix.getType()];
		
		for(int i=0; i<customPixel.length; i++){
			customPixel[i] = pixelValue;
		}
		
		return customPixel;
	}
	
	public static int[] getBlackPixel(Matrix matrix){
		return getCustomPixel(matrix, Matrix.BLACK_PIXEL);
	}
	
	public static int[] getWhitePixel(Matrix matrix){
		return getCustomPixel(matrix, Matrix.BLACK_PIXEL);
	}
	
	//////////////////////////////////////////////////////////////////////
	
	public static Matrix createNewMatrix(Matrix matrix, int initialValue){
		int rows = matrix.getRows(),
			cols = matrix.getCols(),
			type = matrix.getType();
		
		Matrix matrix2 = new Matrix(rows, cols, type);
		for(int i=0; i<rows; i++) {
			for(int j=0; j<cols; j++) {
				for(int k=0; k<type; k++){
					matrix2.pixels[i][j][k] = initialValue;
				}
			}
		}
		
		return matrix2;
	}
	
	public static Matrix createNewMatrix(Matrix matrix){
		return new Matrix(matrix.getRows(), matrix.getCols(), matrix.getType());
	}
	
	//////////////////////////////////////////////////////////////////////
	
	/**
	 * Converts any 2d Matrix to 1d vector.
	 * @param matrix
	 * @return
	 */
	public static int[][] vectorize(Matrix matrix){
		int height = matrix.getRows();
		int width = matrix.getCols();
		int type = matrix.getType();
		
		int[][] vector = new int[height*width][type];
		
		for(int i=0, k=0; i<height; i++){
			for(int j=0; j<width; j++){
				for(int p=0; p<type; p++){
					vector[k][p] = matrix.pixels[i][j][p];
				}
				k++;
			}
		}
		
		return vector;
	}
	
	/**
	 * creates gray-scale square matrix 
	 * @param vector <code>0 => vector[i] <= 1</code>
	 * @return
	 */
	public static Matrix createMatrix(double[] vector){
		
		int row = (int) Math.sqrt(vector.length);
		int col = row;
		
		return createMatrix(vector, row, col);
	}
	
	/**
	 * creates gray-scale matrix
	 * @param vector <code>0 => vector[i] <= 1</code>
	 * @param row
	 * @param col
	 * @return
	 */
	public static Matrix createMatrix(double[] vector, int row, int col){

		Matrix matrix = new Matrix(row, col, Matrix.BLACK_WHITE);
		
		int k=0;
		for(int i=0; i<row; i++){
			for(int j=0; j<col; j++){
				matrix.pixels[i][j] = new int[]{ (int) (vector[k]*254) };
				k++;
			}
		}
		
		return matrix;
	}
	
	/**
	 * creates any type of Matrix from a vector
	 * @param vector
	 * @param row
	 * @param col
	 * @return
	 */
	public static Matrix createMatrix(int[][] vector, int row, int col){

		Matrix matrix = new Matrix(row, col, vector[0].length);
		
		int k=0;
		for(int i=0; i<row; i++){
			for(int j=0; j<col; j++){
				matrix.pixels[i][j] = vector[k];
				k++;
			}
		}
		
		return matrix;
	}
	
	/**
	 * creates a grey scale image
	 * @param matrix2D
	 * @return
	 */
	public static Matrix createMatrix(int[][] matrix2D){
		Matrix matrix = new Matrix(matrix2D.length , matrix2D[0].length, Matrix.BLACK_WHITE);
		
		for(int i=0; i<matrix2D.length; i++){
			for(int j=0; j<matrix2D[i].length; j++){
				matrix.pixels[i][j][0] = matrix2D[i][j];
				
				if(matrix.pixels[i][j][0]>Matrix.WHITE_PIXEL){
					matrix.pixels[i][j][0]=Matrix.WHITE_PIXEL;
				}else if(matrix.pixels[i][j][0]<Matrix.BLACK_PIXEL){
					matrix.pixels[i][j][0]=Matrix.BLACK_PIXEL;
				}
			}
		}
		
		return matrix;
	}
	
	//////////////////////////////////////////////////////////////////////////
	
	public static Matrix multiplyMatrixToMatrix(Matrix matrix1, Matrix matrix2){
		Matrix resultMatrix = createNewMatrix(matrix1);
		
		for(int i=0; i<resultMatrix.pixels.length; i++){
			for(int j=0; j<resultMatrix.pixels[i].length; j++){
				for(int k=0; k<resultMatrix.pixels[i][j].length; k++){
					resultMatrix.pixels[i][j][k] = matrix1.pixels[i][j][k]*matrix2.pixels[i][j][k];
				}
			}
		}
		
		return resultMatrix;
	}
	
	public static Matrix divideMatrixToMatrix(Matrix matrix1, Matrix matrix2){
		Matrix resultMatrix = createNewMatrix(matrix1);
		
		for(int i=0; i<resultMatrix.pixels.length; i++){
			for(int j=0; j<resultMatrix.pixels[i].length; j++){
				for(int k=0; k<resultMatrix.pixels[i][j].length; k++){
					if(matrix2.pixels[i][j][k]==0){
						resultMatrix.pixels[i][j][k]=Matrix.BLACK_PIXEL;
						continue;
					}
					
					resultMatrix.pixels[i][j][k] = matrix1.pixels[i][j][k]/matrix2.pixels[i][j][k];
				}
			}
		}
		
		return resultMatrix;
	}
	
	public static Matrix addMatrixToMatrix(Matrix matrix1, Matrix matrix2){
		Matrix resultMatrix = createNewMatrix(matrix1);
		
		for(int i=0; i<resultMatrix.pixels.length; i++){
			for(int j=0; j<resultMatrix.pixels[i].length; j++){
				for(int k=0; k<resultMatrix.pixels[i][j].length; k++){
					resultMatrix.pixels[i][j][k] = matrix1.pixels[i][j][k]+matrix2.pixels[i][j][k];
				}
			}
		}
		
		return resultMatrix;
	}
	
	public static Matrix substractMatrixToMatrix(Matrix matrix1, Matrix matrix2){
		Matrix resultMatrix = createNewMatrix(matrix1);
		
		for(int i=0; i<resultMatrix.pixels.length; i++){
			for(int j=0; j<resultMatrix.pixels[i].length; j++){
				for(int k=0; k<resultMatrix.pixels[i][j].length; k++){
					resultMatrix.pixels[i][j][k] = matrix1.pixels[i][j][k]-matrix2.pixels[i][j][k];
				}
			}
		}
		
		return resultMatrix;
	}
	
	//////////////////////////////////////////////////////////////////////////////
	
	public static Matrix normalizeMatrix(Matrix matrix){
		int rows = matrix.getRows(),
			cols = matrix.getCols(),
			type = matrix.getType();
		
		int highestPixel = Integer.MIN_VALUE,
			lowestPixel = Integer.MAX_VALUE;
		for(int i=0; i<rows; i++) {
			for(int j=0; j<cols; j++) {
				for(int k=0; k<type; k++){
					if(lowestPixel>matrix.pixels[i][j][k]){
						lowestPixel=matrix.pixels[i][j][k];
					}else if(highestPixel<matrix.pixels[i][j][k]){
						highestPixel=matrix.pixels[i][j][k];
					}
				}
			}
		}
		
		double multiple = (Matrix.WHITE_PIXEL-Matrix.BLACK_PIXEL)/(highestPixel-lowestPixel);
		for(int i=0; i<rows; i++) {
			for(int j=0; j<cols; j++) {
				for(int k=0; k<type; k++){
					matrix.pixels[i][j][k] -= lowestPixel;
					matrix.pixels[i][j][k] = (int) (matrix.pixels[i][j][k]*multiple);
				}
			}
		}
		
		return matrix;
	}
	
}
