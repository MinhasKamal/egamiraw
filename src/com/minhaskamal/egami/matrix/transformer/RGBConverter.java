/************************************************************************************************************
* Developer: Minhas Kamal(minhaskamal024@gmail.com)															*
* Date: Dec-2015																							*
************************************************************************************************************/

package com.minhaskamal.egami.matrix.transformer;

import com.minhaskamal.egami.matrix.Matrix;
import com.minhaskamal.egami.matrix.converter.MatrixTypeConverter;

public class RGBConverter {
	
	public RGBConverter() {
		
	}
	
	/**
	 * Usually this condition should be satisfied- <br/> <code>multiplierOfRed + multiplierOfGreen + 
	 * multiplierOfBlue + (adjustment / 255) <= 1.00 </code>
	 * @param multiplierOfRed
	 * @param multiplierOfGreen
	 * @param multiplierOfBlue
	 * @param adjustment
	 * @return
	 */
	public Matrix transform(Matrix matrix, double multiplierOfRed, double multiplierOfGreen,
			double multiplierOfBlue, int adjustment){
		
		matrix = new MatrixTypeConverter().convert(matrix, Matrix.RED_GREEN_BLUE);
		
		int row = matrix.getRows();
		int col = matrix.getCols();
		
		Matrix matrix2 = new Matrix(row, col, Matrix.BLACK_WHITE);
		
		for(int i=0; i<row; i++){
			for(int j=0; j<col; j++){
				matrix2.pixels[i][j][0] = (int) (matrix.pixels[i][j][0]*multiplierOfRed +
						matrix.pixels[i][j][1]*multiplierOfGreen +
						matrix.pixels[i][j][2]*multiplierOfBlue) + adjustment;
			}
		}
		
		return matrix2;
	}
	
	/**
	 * RGB to YCbCr conversion- returns Y
	 * @return
	 */
	public Matrix transformToY(Matrix matrix){
		double multiplierOfRed = 0.299;
		double multiplierOfGreen = 0.587;
		double multiplierOfBlue = 0.114;
		int adjustment = 0;
		
		return transform(matrix, multiplierOfRed, multiplierOfGreen, multiplierOfBlue, adjustment);
	}
	/**
	 * RGB to YCbCr conversion- returns Cb
	 * @return
	 */
	public Matrix transformToCb(Matrix matrix){
		double multiplierOfRed = -0.16874;
		double multiplierOfGreen = -0.33126;
		double multiplierOfBlue = 0.50000;
		int adjustment = 128;
		
		return transform(matrix, multiplierOfRed, multiplierOfGreen, multiplierOfBlue, adjustment);
	}
	/**
	 * RGB to YCbCr conversion- returns Cr
	 * @return
	 */
	public Matrix transformToCr(Matrix matrix){
		double multiplierOfRed = 0.50000;
		double multiplierOfGreen = -0.41869;
		double multiplierOfBlue = -0.08131;
		int adjustment = 128;
		
		return transform(matrix, multiplierOfRed, multiplierOfGreen, multiplierOfBlue, adjustment);
	}
	
	
	public Matrix transformToHue(Matrix matrix){
		if(matrix.getType()<Matrix.RED_GREEN_BLUE){
			return null;
		}
		
		int row = matrix.getRows();
		int col = matrix.getCols();
		
		Matrix matrix2 = new Matrix(row, col, Matrix.BLACK_WHITE);
		
		int max, mid, min, delta;
		double temp;
		for(int i=0; i<row; i++){
			for(int j=0; j<col; j++){
				max=mid=min=matrix.pixels[i][j][0];

				for(int k=1; k<3; k++){
					if(max < matrix.pixels[i][j][k]){
						max = matrix.pixels[i][j][k];
					}else if(min > matrix.pixels[i][j][k]){
						min = matrix.pixels[i][j][k];
					}
					mid += matrix.pixels[i][j][k];
				}
				delta = max-min;
				mid = mid-(max+min);
				
				if(delta>0){
					temp = (mid-min+0.00)/delta;

					if(max==matrix.pixels[i][j][0]){		//red
						temp %= 6;
					}else if(max==matrix.pixels[i][j][1]){	//green
						temp += 2;
					}else{									//blue
						temp += 4;
					}
					
				}else{
					temp = 0;
				}
				
				matrix2.pixels[i][j][0] = (int)(temp*42.5);
			}
		}
		
		return matrix2;
	}
	
	public Matrix transformToSaturation(Matrix matrix){
		if(matrix.getType()<Matrix.RED_GREEN_BLUE){
			return null;
		}
		
		int row = matrix.getRows();
		int col = matrix.getCols();
		
		Matrix matrix2 = new Matrix(row, col, Matrix.BLACK_WHITE);
		
		int max, min, delta;
		for(int i=0; i<row; i++){
			for(int j=0; j<col; j++){
				max=min=matrix.pixels[i][j][0];
				for(int k=1; k<3; k++){
					if(max < matrix.pixels[i][j][k]){
						max = matrix.pixels[i][j][k];
					}else if(min > matrix.pixels[i][j][k]){
						min = matrix.pixels[i][j][k];
					}
				}
				delta = max-min;
				
				if(delta>0){
					matrix2.pixels[i][j][0] = ((delta*255)/max);
				}else{
					matrix2.pixels[i][j][0] = 0;
				}
			}
		}
		
		return matrix2;
	}
	
	public Matrix transformToValue(Matrix matrix){
		if(matrix.getType()<Matrix.RED_GREEN_BLUE){
			return null;
		}
		
		int row = matrix.getRows();
		int col = matrix.getCols();
		
		Matrix matrix2 = new Matrix(row, col, Matrix.BLACK_WHITE);
		
		int max;
		for(int i=0; i<row; i++){
			for(int j=0; j<col; j++){
				max=matrix.pixels[i][j][0];
				for(int k=1; k<3; k++){
					if(max < matrix.pixels[i][j][k]){
						max = matrix.pixels[i][j][k];
					}
				}
				
				matrix2.pixels[i][j][0] = max;
			}
		}
		
		return matrix2;
	}
	
	public Matrix transformToLightness(Matrix matrix){
		if(matrix.getType()<Matrix.RED_GREEN_BLUE){
			return null;
		}
		
		int row = matrix.getRows();
		int col = matrix.getCols();
		
		Matrix matrix2 = new Matrix(row, col, Matrix.BLACK_WHITE);
		
		int max, min;
		for(int i=0; i<row; i++){
			for(int j=0; j<col; j++){
				max=min=matrix.pixels[i][j][0];
				for(int k=1; k<3; k++){
					if(max < matrix.pixels[i][j][k]){
						max = matrix.pixels[i][j][k];
					}else if(min > matrix.pixels[i][j][k]){
						min = matrix.pixels[i][j][k];
					}
				}
				
				matrix2.pixels[i][j][0] = (max+min)/2;
			}
		}
		
		return matrix2;
	}
	
}
