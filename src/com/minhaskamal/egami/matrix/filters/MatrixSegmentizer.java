package com.minhaskamal.egami.matrix.filters;

import java.util.ArrayList;

import com.minhaskamal.egami.matrix.Matrix;

public class MatrixSegmentizer {
	public int[] verticalHistogram(Matrix matrix, int threshold){
		int[] histogram = new int[matrix.pixels.length];
		
		for(int i=0; i<matrix.pixels.length; i++){
			for(int j=0; j<matrix.pixels[i].length; j++){
				if(matrix.pixels[i][j][0]<=threshold){
					histogram[i]++;
				}
			}
		}
		
		return histogram;
	}
	
	public Matrix[] verticallySegmentize(Matrix matrix, int[] histogram, int minIntensity){
		ArrayList<Matrix> matrixes = new ArrayList<>();
		
		int rowStart = 0;
		int rowEnd = 0;
		boolean isInsideSegment = false;
		for(int i=0; i<histogram.length; i++){
			
			if(histogram[i]>=minIntensity){
				if(!isInsideSegment){
					rowStart = i;
					isInsideSegment = true;
				}else{
					rowEnd = i;
				}
			}else if(isInsideSegment && rowStart<rowEnd){
				Matrix subMatrix = matrix.subMatrix(rowStart, rowEnd, 0, matrix.getCols()-1);
				matrixes.add(subMatrix);
				isInsideSegment = false;
			}
		}
		
		return matrixes.toArray(new Matrix[0]);
	}
	
	/*public int[] upDownRange(Matrix mat, int startPosition){
		int length = mat.getCols(), height = mat.getRows();
		int[] y = new int[2];
		
		boolean blackDotFound = false;
		int position, i;
		for(position=startPosition+1; position<height && !blackDotFound; position++){
			for(i=0; i<length; i++){
				if(mat.pixels[position][i][0]<darkValue){
					blackDotFound=true;
					break;
				}
			}
		}
		
		if(!blackDotFound){
			return y;
		}
		
		y[0] = position-1-buffer;
		
		for(; position<height && blackDotFound; position++){
			blackDotFound=false;
			for(i=0; i<length; i++){
				if(mat.pixels[position][i][0]<darkValue){
					blackDotFound=true;
					break;
				}
			}
		}
		
		y[1] = position-1+buffer;
		
		return y;
	}*/
}
