/************************************************************************************************************
* Developer: Minhas Kamal(minhaskamal024@gmail.com)															*
* Date: 01-Apr-2016																							*
************************************************************************************************************/

package com.minhaskamal.egami.matrix.filters;

import java.util.ArrayList;
import java.util.LinkedList;

import com.minhaskamal.egami.matrix.Matrix;
import com.minhaskamal.egami.matrix.converter.MatrixTypeConverter;
import com.minhaskamal.egami.matrix.editor.MatrixBorderCreator;
import com.minhaskamal.egami.matrix.utils.MatrixUtils;

public class MatrixPixelGroupExtractor {

	private Matrix matrix;
	private ArrayList<ArrayList<int[]>> connectedPixelGroups;
	
	public MatrixPixelGroupExtractor(Matrix matrix) {
		this.matrix = new MatrixTypeConverter().convert(matrix, Matrix.BLACK_WHITE);
		this.connectedPixelGroups = new ArrayList<>();
	}
	
	public void extractPixelGroup(int threshold,
			int minWidth, int minHeight, int maxWidth, int maxHeight,
			int minNumberOfPixels, int maxNumberOfPixels, int minPixelPercentage, int maxPixelPercentage){
		
		int rows = this.matrix.getRows(),
			cols = this.matrix.getCols();
		
		
		Matrix borderedMatrix = new MatrixBorderCreator().createBorder(this.matrix, 1, Matrix.WHITE_PIXEL);
		
		for(int i=0; i<rows; i++){
			for(int j=0; j<cols; j++){

				if(borderedMatrix.pixels[i][j][0] < threshold){
					ArrayList<int[]> connectedDots = getConnectedPixelGroup(borderedMatrix, threshold, i, j);
					int[] minXMaxXMinYMaxY = MatrixUtils.getMinXMaxXMinYMaxY(connectedDots);
					
					int width = minXMaxXMinYMaxY[1]-minXMaxXMinYMaxY[0]+1,
						height = minXMaxXMinYMaxY[3]-minXMaxXMinYMaxY[2]+1,
						numberOfPixels = connectedDots.size(),
						pixelPercentage = numberOfPixels*100/(width*height);
					
					if(width>minWidth && width<maxWidth && height>minHeight && height<maxHeight &&
							numberOfPixels>minNumberOfPixels && numberOfPixels<maxNumberOfPixels &&
							pixelPercentage>minPixelPercentage && pixelPercentage<maxPixelPercentage){
						
						this.connectedPixelGroups.add(connectedDots);
					}
				}
				
			}
		}
		
		return ;
	}
	
	private ArrayList<int[]> getConnectedPixelGroup(Matrix matrix, int threshold, int i, int j){
		ArrayList<int[]> blackDots = new ArrayList<>();
		LinkedList<int[]> blackDotStack = new LinkedList<>();

		blackDotStack.addLast(new int[]{i, j});
		matrix.pixels[i][j][0] = Matrix.WHITE_PIXEL;
		while(!blackDotStack.isEmpty()){
			int[] blackDot = blackDotStack.pop();
			blackDots.add(blackDot);

			for(int k=-1; k<2; k++){
				for(int l=-1; l<2; l++){

					if(matrix.pixels[blackDot[0]+k][blackDot[1]+l][0] < threshold){
						blackDotStack.addLast( new int[]{blackDot[0]+k, blackDot[1]+l} );
						matrix.pixels[blackDot[0]+k][blackDot[1]+l][0] = Matrix.WHITE_PIXEL;
					}
				}
			}
		}

		return blackDots;
	}
	
	/////////////////////////////////////////////////////////////////////////////////////
	
	public ArrayList<ArrayList<int[]>> getConnectedPixelGroups(){
		return connectedPixelGroups;
	}
	
	public ArrayList<int[]> getConnectedPixelGroupPositions(){
		ArrayList<int[]> connectedPixelGroupPositions = new ArrayList<>();
		
		for(int i=0; i<connectedPixelGroups.size(); i++){
			connectedPixelGroupPositions.add(connectedPixelGroups.get(i).get(0));
		}
		
		return connectedPixelGroupPositions;
	}
	
	public Matrix getFilteredMatrix(){
		int rows = this.matrix.getRows(),
			cols = this.matrix.getCols();
		
		Matrix filteredMatrix = new Matrix(rows, cols, Matrix.BLACK_WHITE);
		for(int i=0; i<rows; i++){
			for(int j=0; j<cols; j++){
				filteredMatrix.pixels[i][j][0] = Matrix.WHITE_PIXEL;
			}
		}
		
		for(int i=0; i<connectedPixelGroups.size(); i++){
			drawPixelGroup(filteredMatrix, connectedPixelGroups.get(i), Matrix.BLACK_PIXEL);
		}
		
		return filteredMatrix;
	}
	
	private void drawPixelGroup(Matrix matrix, ArrayList<int[]> connectedDots, int pixel){
		for(int[] connectedDot: connectedDots){
			matrix.pixels[connectedDot[0]-1][connectedDot[1]-1][0] = pixel;
		}
	}
	
}
