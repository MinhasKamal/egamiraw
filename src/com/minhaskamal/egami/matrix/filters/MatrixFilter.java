package com.minhaskamal.egami.matrix.filters;

import java.util.Arrays;

import com.minhaskamal.egami.matrix.Matrix;
import com.minhaskamal.egami.matrix.editor.MatrixBorderCreator;
import com.minhaskamal.egami.matrix.utils.MatrixUtils;

public class MatrixFilter {
	public Matrix runMedianFilter(Matrix matrix, int maskLength){
		Matrix matrix2 = new Matrix(matrix.getRows(), matrix.getCols(), Matrix.BLACK_WHITE);
		
		int border = (maskLength-1)/2;
		matrix = new MatrixBorderCreator().createBorder(matrix, border);
		
		int rows = matrix.getRows(),
			cols = matrix.getCols();
		
		for(int i=border; i<rows-border; i++) {
			for(int j=border; j<cols-border; j++) {
				
				int[][] arrayRaw = MatrixUtils.vectorize(
						matrix.subMatrix(i-border, i+maskLength-border, j-border, j+maskLength-border));
				int[] array = new int[arrayRaw.length];
				for(int k=0; k<array.length; k++){
					array[k] = arrayRaw[k][0];
				}
				
				matrix2.pixels[i-border][j-border][0] = getMedian(array);
			}	
		}
		
		return matrix2;
	}
	
	private int getMedian(int[] array){
		
		/*for(int i=0; i<array.length; i++){
			System.out.print(i + " ");
		}System.out.println();*/
		
		Arrays.sort(array);
		
		/*for(int i=0; i<array.length; i++){
			System.out.print(i + "-");
		}System.out.println();*/
		
		int median = array[array.length/2];
		//System.out.println(median);
		
		return median;
	}
	
}
