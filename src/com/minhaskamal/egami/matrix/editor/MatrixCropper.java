/************************************************************************************************************
* Developer: Minhas Kamal(minhaskamal024@gmail.com)															*
* Date: Dec-2015																							*
************************************************************************************************************/

package com.minhaskamal.egami.matrix.editor;

import com.minhaskamal.egami.matrix.Matrix;

public class MatrixCropper {
	public MatrixCropper() {
		
	}
	
	
	public Matrix crop(Matrix matrix, int top, int right, int down, int left){
		return matrix.subMatrix(top, matrix.getRows()-down, left, matrix.getCols()-right);
	}
	
}
