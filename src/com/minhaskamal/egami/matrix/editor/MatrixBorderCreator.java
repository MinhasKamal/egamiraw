/************************************************************************************************************
* Developer: Minhas Kamal(minhaskamal024@gmail.com)															*
* Date: Dec-2015																							*
************************************************************************************************************/

package com.minhaskamal.egami.matrix.editor;

import com.minhaskamal.egami.matrix.Matrix;

public class MatrixBorderCreator {
	
	public MatrixBorderCreator() {
		
	}

	public Matrix createBorder(Matrix matrix, int breadth){	
		return createBorder(matrix, breadth, Matrix.WHITE_PIXEL);
	}
	
	public Matrix createBorder(Matrix matrix, int breadth, int pixelValue){
		int[] borderPixel = new int[matrix.getType()];
		for(int i=0; i<borderPixel.length; i++){
			borderPixel[i] = pixelValue;
		}
		
		return createBorder(matrix, breadth, breadth, borderPixel);
	}
	
	/**
	 * 
	 * @param horizontalBreadth left and right side breadth
	 * @param verticalBreadth top and bottom side breadth
	 * @param borderPixel
	 * @return
	 */
	public Matrix createBorder(Matrix matrix, int horizontalBreadth, int verticalBreadth, int borderPixel[]){
		
		return createBorder(matrix, verticalBreadth, horizontalBreadth, verticalBreadth, horizontalBreadth, borderPixel);
	}
	
	public Matrix createBorder(Matrix matrix, int topBreadth, int rightBreadth, int bottomBreadth, int leftBreadth,
			int borderPixel[]){
		if(borderPixel.length!=matrix.getType()){
			return matrix;
		}
		
		int rows = matrix.getRows(),
			cols = matrix.getCols();
			
		int newRows = rows+topBreadth+bottomBreadth,
			newCols = cols+rightBreadth+leftBreadth;
		
		Matrix matrix2 = new Matrix(newRows, newCols, matrix.getType());
		
		for(int i=0; i<newRows; i++){
			for(int j=0; j<newCols; j++){
				matrix2.pixels[i][j] = borderPixel;//###
			}
		}
		
		for(int i=0; i<rows; i++){
			for(int j=0; j<cols; j++){
				matrix2.pixels[i+topBreadth][j+leftBreadth] = matrix.pixels[i][j];
			}
		}
		
		return matrix2;
	}
	
}
