/************************************************************************************************************
* Developer: Minhas Kamal(minhaskamal024@gmail.com)															*
* Date: Dec-2015																							*
************************************************************************************************************/

package com.minhaskamal.egami.matrix.editor;

import com.minhaskamal.egami.matrix.Matrix;

public class MatrixRotator {
	
	public MatrixRotator() {
		
	}
	
	///full rotate///
	
	/**
	 * Rotate 90 degree to left.
	 * @param matrix
	 * @return
	 */
	public Matrix rotateLeft(Matrix matrix){
		int newRow = matrix.getCols();
		int newCol = matrix.getRows();
		
		Matrix matrix2 = new Matrix(newRow, newCol, matrix.getType());
		
		for(int i=0; i<newRow; i++){
			for(int j=0; j<newCol; j++){
				matrix2.pixels[newRow-1-i][j] = matrix.pixels[j][i].clone();
			}
		}
		
		return matrix2;
	}
	
	/**
	 * Rotate 90 degree to right.
	 * @param matrix
	 * @return
	 */
	public Matrix rotateRight(Matrix matrix){
		int newRow = matrix.getCols();
		int newCol = matrix.getRows();
		
		Matrix matrix2 = new Matrix(newRow, newCol, matrix.getType());
		
		for(int i=0; i<newRow; i++){
			for(int j=0; j<newCol; j++){
				matrix2.pixels[i][newCol-1-j] = matrix.pixels[j][i].clone();
			}
		}
		
		return matrix2;
	}
	
	///flipper///
	
	/**
	 * left-right flip
	 * @return
	 */
	public Matrix flipVertical(Matrix matrix){
		int row = matrix.getRows();
		int col = matrix.getCols();
		
		Matrix matrix2 = new Matrix(row, col, matrix.getType());
		
		for(int i=0; i<row; i++){
			for(int j=0; j<col; j++){
				matrix2.pixels[i][col-j-1] = matrix.pixels[i][j].clone();
			}
		}
		
		return matrix2;
	}
	
	/**
	 * top-bottom flip
	 * @return
	 */
	public Matrix flipHorizontal(Matrix matrix){
		int row = matrix.getRows();
		int col = matrix.getCols();
		
		Matrix matrix2 = new Matrix(row, col, matrix.getType());
		
		for(int i=0; i<row; i++){
			for(int j=0; j<col; j++){
				matrix2.pixels[row-i-1][j] = matrix.pixels[i][j].clone();
			}
		}
		
		return matrix2;
	}

	///skewer///
	
	
	public Matrix skewHorizontal(Matrix matrix, double angle){
		angle = angle%90;
		
		int row = matrix.getRows();
		int col = matrix.getCols();
		
		double tanX = Math.tan(Math.toRadians(angle));
		
		Matrix matrix2 = new Matrix(row, (int)(col + ( row*Math.abs(tanX) )), matrix.getType());
		
		int x=0;
		if(angle<0){
			x = (int) (row*Math.abs(tanX));
		}
		for(int i=0; i<row; i++){
			int skew = (int) (i*tanX);
			for(int j=0; j<col; j++){
				matrix2.pixels[i][j+skew+x] = matrix.pixels[i][j].clone();
			}
		}
		
		return matrix2;
	}
	
	
	public Matrix skewVertical(Matrix matrix, double angle){
		angle = angle%90;
		
		int row = matrix.getRows();
		int col = matrix.getCols();
		
		double tanX = Math.tan(Math.toRadians(angle));
		
		Matrix matrix2 = new Matrix((int)(row + ( col*Math.abs(tanX) )), col, matrix.getType());
		
		int x=0;
		if(angle<0){
			x = (int) (col*Math.abs(tanX));
		}
		for(int j=0; j<col; j++){
			int skew = (int) ((col-1-j)*tanX);
			for(int i=0; i<row; i++){
				matrix2.pixels[i+skew+x][j] = matrix.pixels[i][j].clone();
			}
		}
		
		return matrix2;
	}
	
	
	///rotate angle///
	/**
	 * works with simple algorithm
	 * @param angle in degree
	 */
	public Matrix rotate(Matrix matrix, double angle){
		angle = primaryRotation(matrix, angle);
		if(angle==0){
			return matrix;
		}
		
		int row = matrix.getRows();
		int col = matrix.getCols();
		
		double sinX = Math.sin(Math.toRadians(angle));
		double cosX = Math.cos(Math.toRadians(angle));
		
		double height_sinX = row*sinX;
		
		Matrix matrix2 = new Matrix( (int)(row*cosX+col*sinX), (int)(height_sinX+col*cosX), matrix.getType() );
		
		for(int i=0, j, newI, newJ; i<row; i++){
			for(j=0; j<col; j++){
				newI = (int) Math.floor(i*cosX + j*sinX);
				newJ = (int) Math.floor(-i*sinX + j*cosX + height_sinX);
				
				matrix2.pixels[newI][newJ] =  matrix.pixels[i][j].clone();
			}
		}
		
		return matrix2;
	}
	
	/**
	 * 
	 * @param matrix
	 * @param angle
	 * @return
	 */
	public Matrix rotate2(Matrix matrix, double angle){
		angle = -primaryRotation(matrix, angle);
		if(angle==0){
			return matrix;
		}
		
		///////////////////////////////////////////
		double sinX = Math.sin(Math.toRadians(angle));
		double cosX = Math.cos(Math.toRadians(angle));
		
		int row = matrix.getRows();
		int col = matrix.getCols();
		
		int rotatedRows = (int) ( row*Math.abs(cosX) + col*Math.abs(sinX) );
		int rotatedCols = (int) ( row*Math.abs(sinX) + col*Math.abs(cosX) );
		
		///////////////////////////////////////////
		matrix = skewVertical(matrix, angle/2);
		
		///////
		row = matrix.getRows();
		col = matrix.getCols();
		
		Matrix matrix2 = new Matrix(row, (int)(col + ( row*Math.abs(sinX) )), matrix.getType());
		
		int x=0;
		if(angle<0){
			x = (int) (row*Math.abs(sinX));
		}
		for(int i=0; i<row; i++){
			int skew = (int) (i*sinX);
			for(int j=0; j<col; j++){
				matrix2.pixels[i][j+skew+x] = matrix.pixels[i][j].clone();
			}
		}
		
		///////
		matrix2 = skewVertical(matrix2, angle/2);
		
		//////////////////////////////////////////
		
		int horizontalCropSize = (int) ( (matrix2.getCols()-rotatedCols)/2 );
		int verticalCropSize = (int) ( (matrix2.getRows()-rotatedRows)/2 );
	
		matrix2 = new MatrixCropper().crop(matrix2,
				horizontalCropSize, horizontalCropSize, verticalCropSize, verticalCropSize);

		return matrix2;
	}
	
	/**
	 * 
	 * @param matrix
	 * @param angle
	 * @return
	 */
	private double primaryRotation(Matrix matrix, double angle){
		angle = angle%360;
		if(angle<0){
			angle = 360+angle;
		}
		
		if(angle>=270){
			matrix = rotateLeft(matrix);
		}else if(angle>=180){
			matrix = rotateRight(rotateRight(matrix));
		}else if(angle>=90){
			matrix = rotateRight(matrix);
		}
		
		angle = angle%90;
		
		return angle;
	}

}
