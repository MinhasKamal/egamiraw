/************************************************************************************************************
* Developer: Minhas Kamal(minhaskamal024@gmail.com)															*
* Date: Dec-2015																							*
************************************************************************************************************/

package com.minhaskamal.egami.matrix.editor;

import com.minhaskamal.egami.matrix.Matrix;
import com.minhaskamal.egami.matrixMath.MatrixMath;

public class MatrixResizer {
	
	public MatrixResizer() {
		
	}
	
	
	public Matrix horizontallyStretch(Matrix matrix, double percentage){
		int rows = matrix.getRows(),
			cols = matrix.getCols();
			
		double multiplier = percentage/100;
		
		int newRows = rows,
			newCols = (int)(cols*multiplier);
		Matrix matrix2 = new Matrix(newRows, newCols, matrix.getType());
		
		multiplier += multiplier/(cols-1);	//mild correction
		
		double[] pixelDiff = new double[matrix.getType()];
		for(int i=0, j; i<rows; i++){
			for(j=1; j<cols; j++){
				int[] initialPixel = matrix.pixels[i][j-1].clone();
				int initialColumn = (int) ((j-1)*multiplier);
				double finalColumn = j*multiplier;
				
				pixelDiff = new MatrixMath().divideArrayByNumber(
						new MatrixMath().subtracArrayByArray(initialPixel, matrix.pixels[i][j]),
						multiplier);
				
				matrix2.pixels[i][initialColumn] = initialPixel;
				for(int k=initialColumn+1; k<finalColumn; k++){
					matrix2.pixels[i][k] = arr2arrAddition(matrix2.pixels[i][k-1], pixelDiff);
				}
			}
		}
		
		return matrix2;
	}
	
	
	public Matrix verticallyStretch(Matrix matrix, double percentage){
		int rows = matrix.getRows(),
			cols = matrix.getCols();
			
		double multiplier = percentage/100;
		
		int newRows = (int)(rows*multiplier),
			newCols = cols;
		Matrix matrix2 = new Matrix(newRows, newCols, matrix.getType());
		
		multiplier += multiplier/(rows-1);	//mild correction
		
		double[] pixelDiff = new double[matrix.getType()];
		for(int i=1, j; i<rows; i++){
			for(j=0; j<cols; j++){
				int[] initialPixel = matrix.pixels[i-1][j];
				int initialRow = (int) ((i-1)*multiplier);
				double finalRow = i*multiplier;
				
				pixelDiff = new MatrixMath().divideArrayByNumber(
						new MatrixMath().subtracArrayByArray(initialPixel, matrix.pixels[i][j]),
						multiplier);
				
				matrix2.pixels[initialRow][j] = initialPixel;
				for(int k=initialRow+1; k<finalRow; k++){
					matrix2.pixels[k][j] = arr2arrAddition(matrix2.pixels[k-1][j], pixelDiff);
				}
				
			}
		}
		
		return matrix2;
	}
	
	private int[] arr2arrAddition(int[] array1, double[] array2){
		int[] resultArray = new int [array1.length];
		
		for(int i=0; i<resultArray.length; i++){
			resultArray[i] = (int) (array1[i]+array2[i]);
			if(resultArray[i]>255){
				resultArray[i]=255;
			}else if(resultArray[i]<0){
				resultArray[i]=0;
			}
		}
		
		return resultArray;
	}
	
	
	public Matrix resize(Matrix matrix, double horizontalPercentage, double verticalPercentage){
		matrix = horizontallyStretch(matrix, horizontalPercentage);
		matrix = verticallyStretch(matrix, verticalPercentage);
		
		return matrix;
	}
	
	public Matrix resize(Matrix matrix, double percentage){
		return resize(matrix, percentage, percentage);
	}
	
	
	public Matrix resize(Matrix matrix, int length, int width){
		
		double horizontalPercentage = length*100/matrix.getCols(),
			verticalPercentage = width*100/matrix.getRows();
		
		return resize(matrix, horizontalPercentage, verticalPercentage);
	}
	
}
