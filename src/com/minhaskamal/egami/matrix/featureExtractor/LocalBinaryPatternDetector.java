/************************************************************************************************************
* Developer: Minhas Kamal(minhaskamal024@gmail.com)															*
* Date: Dec-2015																							*
************************************************************************************************************/

package com.minhaskamal.egami.matrix.featureExtractor;

import com.minhaskamal.egami.matrix.Matrix;
import com.minhaskamal.egami.matrix.converter.MatrixTypeConverter;

public class LocalBinaryPatternDetector {
	public LocalBinaryPatternDetector() {
		
	}
	
	/**
	 * Applies LBP over grey scale image.
	 * @param mat
	 * @return
	 */
	public Matrix extractLocalBinaryPattern(Matrix mat){
		mat = new MatrixTypeConverter().convert(mat, Matrix.BLACK_WHITE);
		
		int rows = mat.getRows()-1,
			cols = mat.getCols()-1;
			
		//create new matrix//
		Matrix newmat = new Matrix(rows-2, cols-2, Matrix.BLACK_WHITE);
		
		int[] pixel = new int[1];
		int[] LocalBinaryValue = new int[8];
		for(int x=1, y; x<rows-2; x++){
			for(y=1; y<cols-2; y++){
				
				LocalBinaryValue[0] = comaprePixel(mat.pixels[x+1][y+1][0], mat.pixels[x][y][0]);
				LocalBinaryValue[1] = comaprePixel(mat.pixels[x+1][y+1][0], mat.pixels[x+1][y][0]);
				LocalBinaryValue[2] = comaprePixel(mat.pixels[x+1][y+1][0], mat.pixels[x+2][y][0]);

				LocalBinaryValue[3] = comaprePixel(mat.pixels[x+1][y+1][0], mat.pixels[x][y+1][0]);
				LocalBinaryValue[4] = comaprePixel(mat.pixels[x+1][y+1][0], mat.pixels[x+2][y+1][0]);

				LocalBinaryValue[5] = comaprePixel(mat.pixels[x+1][y+1][0], mat.pixels[x][y+2][0]);
				LocalBinaryValue[6] = comaprePixel(mat.pixels[x+1][y+1][0], mat.pixels[x+1][y+2][0]);
				LocalBinaryValue[7] = comaprePixel(mat.pixels[x+1][y+1][0], mat.pixels[x+2][y+2][0]);

				int value = 0;
				for(int i=0; i<8; i++){
					value += LocalBinaryValue[i]*Math.pow(2, 7-i);
				}
				
				pixel[0] = value;
				
				newmat.pixels[x][y] = pixel.clone();
			}
		}
		
		return newmat;
	}
	
	private int comaprePixel(int pixel1, int pixel2){
		//if(pixel2-pixel1 > Math.sqrt(pixel1)){
		if(pixel2-pixel1 > 0){
			return 1;
		}
		
		return 0;
	}
	
}
