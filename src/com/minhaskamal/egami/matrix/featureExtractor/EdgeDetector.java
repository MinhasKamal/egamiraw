/************************************************************************************************************
* Developer: Minhas Kamal(minhaskamal024@gmail.com)															*
* Date: Dec-2015																							*
************************************************************************************************************/

package com.minhaskamal.egami.matrix.featureExtractor;

import com.minhaskamal.egami.matrix.Matrix;
import com.minhaskamal.egami.matrix.converter.MatrixTypeConverter;
import com.minhaskamal.egami.matrix.enhancer.MatrixMasker;

public class EdgeDetector {
	public EdgeDetector() {
		
	}
	
	public Matrix applyEdgeDetecton(Matrix matrix){
		MatrixMasker matrixMasker = new MatrixMasker();
		
		//matrix = matrixMasker.applyMask(matrix, matrixMasker.getGaussianFilter(5, 1));
		Matrix matrixEdgeX = matrixMasker.applyMask(matrix, matrixMasker.getSobelFilter(0));
		Matrix matrixEdgeY = matrixMasker.applyMask(matrix, matrixMasker.getSobelFilter(90));
		
		for(int i=0; i<matrix.pixels.length; i++){
			for(int j=0; j<matrix.pixels[i].length; j++){
				for(int k=0; k<matrix.pixels[i][j].length; k++){
					matrix.pixels[i][j][k] = (int) Math.sqrt(
							matrixEdgeX.pixels[i][j][k]*matrixEdgeX.pixels[i][j][k] +
							matrixEdgeY.pixels[i][j][k]*matrixEdgeY.pixels[i][j][k] );
					
					if(matrix.pixels[i][j][k]<Matrix.BLACK_PIXEL){
						matrix.pixels[i][j][k]=Matrix.BLACK_PIXEL;
					}else if(matrix.pixels[i][j][k]>Matrix.WHITE_PIXEL){
						matrix.pixels[i][j][k]=Matrix.WHITE_PIXEL;
					}
				}
			}
		}
		
		return matrix;
	}
	
	/**
	 * Shows edges on an grey scale image. Works nice on quantized images.
	 * @param mat input <code>Matrix</code> of type grey scale
	 * @param threshold least difference of two pixel to be recognized as a border
	 * @return <code>Matrix</code> containing edges only
	 */
	public Matrix applyEdgeDetecton2(Matrix mat, int threshold){
		mat = new MatrixTypeConverter().convert(mat, Matrix.BLACK_WHITE);
		
		int rows = mat.getRows()-1,
			cols = mat.getCols()-1;
			
		//create new mat//
		Matrix newmat = new Matrix(rows, cols, Matrix.BLACK_WHITE);
		
		int[] pixel = new int[1];
		for(int x=1, y; x<rows; x++){
			for(y=1; y<cols; y++){
				
				if(absolute( mat.pixels[x][y][0]-mat.pixels[x-1][y][0] ) > threshold){
					pixel[0]=254;
				}else if(absolute( mat.getPixel(x, y)[0]-mat.getPixel(x, y-1)[0] ) > threshold){
					pixel[0]=254;
//				}else if(absolute( mat.getPixel(x-1, y-1)[0]-mat.getPixel(x+1, y+1)[0] ) > limit){
//					pixel[0]=254;
//				}else if(absolute( mat.getPixel(x-1, y+1)[0]-mat.getPixel(x+1, y-1)[0] ) > limit){
//					pixel[0]=254;
				}else{
					pixel[0]=0;
				}
				
				newmat.pixels[x][y] =  pixel.clone();
			}
		}
		
		return newmat;
	}
	
	/**
	 * converts double into absolute value.
	 * @param a input value
	 * @return absolute value of a
	 */
	private double absolute(double a){
		if(a>=0){
			return a;
		}else{
			return -a;
		}
	}
	
}
