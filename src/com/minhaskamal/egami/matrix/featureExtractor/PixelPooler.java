/************************************************************************************************************
* Developer: Minhas Kamal(minhaskamal024@gmail.com)															*
* Date: Dec-2015																							*
************************************************************************************************************/

package com.minhaskamal.egami.matrix.featureExtractor;

import com.minhaskamal.egami.matrix.Matrix;
import com.minhaskamal.egami.matrix.converter.MatrixTypeConverter;

public class PixelPooler {
	public PixelPooler() {
		
	}
	
	public Matrix applyMinPooling(Matrix matrix, int regionWidth, int regionHeight) {
		matrix = new MatrixTypeConverter().convert(matrix, Matrix.BLACK_WHITE);
		
		int rows = matrix.getRows(),
			cols = matrix.getCols(),
			newRow = (int) Math.ceil(rows*1.0/regionHeight),
			newCol = (int) Math.ceil(cols*1.0/regionWidth);
			
		Matrix matrix2 = new Matrix(newRow, newCol, Matrix.BLACK_WHITE);

		int newpixel;
		for(int i=0; i<newRow; i++) {
			for(int j=0; j<newCol; j++) {

				newpixel = 255;	//min pooling init

				for(int m=i*regionHeight; m<(i+1)*regionHeight && m<rows; m++){
					for(int n=j*regionWidth; n<(j+1)*regionWidth && n<cols; n++){
						if(newpixel>matrix.pixels[m][n][0]){	//min pooling
							newpixel = matrix.pixels[m][n][0];
						}
					}
				}

				matrix2.pixels[i][j][0] = newpixel;
			}
		}

		return matrix2;
	}
	
}
