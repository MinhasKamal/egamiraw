package com.minhaskamal.egami.matrix.featureExtractor;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;

import com.minhaskamal.egami.matrix.Matrix;
import com.minhaskamal.egami.matrix.converter.MatrixTypeConverter;

public class HoughTransformer {
	private int angleRangeStart;
	private int angleRangeEnd;
	
	public HoughTransformer(int angleRangeStart, int angleRangeEnd) {
		this.angleRangeStart = angleRangeStart;
		this.angleRangeEnd = angleRangeEnd;
	}
	
	public ArrayList<int[]> applyHoughTransformer(Matrix matrix, int classesOfAngles, int numberOfLines){
		matrix = new MatrixTypeConverter().convert(matrix, Matrix.BLACK_WHITE);
		
		//EdgeDetector edgeDetector = new EdgeDetector();
		//matrix=edgeDetector.applyEdgeDetecton(matrix);
		
		ArrayList<int[]> lines = detectLines(matrix, classesOfAngles, numberOfLines);
		
//		//test
//		for(int i=0; i<lines.size(); i++){
//			System.out.println(lines.get(i)[0]+" "+lines.get(i)[1]+" "+lines.get(i)[2]);
//		}
		
		return lines;
	}
	
	private ArrayList<int[]> detectLines(Matrix matrix, int classesOfAngles, int numberOfLines){
		
		int possibleLines[][] = countVotes(matrix, classesOfAngles);

		ArrayList<int[]> actualLines = new ArrayList<>();	//{votes, distance, angle}
		for(int i=0; i<numberOfLines; i++){
			actualLines.add(new int[]{0, 0, 0});
		}
		
		double UnitAngle = (angleRangeEnd-angleRangeStart)/classesOfAngles;
		for(int i=0; i<possibleLines.length; i++){
			for(int j=0; j<possibleLines[i].length; j++){
				if(possibleLines[i][j]>actualLines.get(0)[0]){
					actualLines.get(0)[0] = possibleLines[i][j];
					actualLines.get(0)[1] = i;
					actualLines.get(0)[2] = (int)(UnitAngle*j)+angleRangeStart;
					
					Collections.sort(actualLines, new Comparator<int[]>() {
						@Override
						public int compare(int[] arg0, int[] arg1) {
							return arg0[0]-arg1[0];
						}
					});
				}
			}
		}
		
		return actualLines;
	}
	
	private int[][] countVotes(Matrix matrix, int classesOfAngles){
		double UnitAngle = Math.toRadians(angleRangeEnd-angleRangeStart)/classesOfAngles;
		int maxLineDistance = (int) Math.sqrt( Math.pow(matrix.getRows(), 2) + Math.pow(matrix.getCols(), 2) )+1;
		
		int possibleLines[][] = new int[maxLineDistance][classesOfAngles];
		for(int i=0; i<matrix.pixels.length; i++){
			for(int j=0; j<matrix.pixels[i].length; j++){
				if(matrix.pixels[i][j][0]<150){
					
					for(int k=0; k<classesOfAngles; k++){
						double angle = UnitAngle*k + Math.toRadians(angleRangeStart);
						int distance = Math.abs((int) (j*Math.cos(angle)+i*Math.sin(angle)));
						possibleLines[distance][k] += 1;
					}
				}
			}
		}
		
		return possibleLines;
	}
}
