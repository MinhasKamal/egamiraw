/************************************************************************************************************
* Developer: Minhas Kamal(minhaskamal024@gmail.com)															*
* Date: Dec-2015																							*
************************************************************************************************************/

package com.minhaskamal.egami.matrix.enhancer;

import com.minhaskamal.egami.matrix.Matrix;
import com.minhaskamal.egami.matrix.converter.MatrixTypeConverter;
import com.minhaskamal.egami.matrix.utils.MatrixUtils;

public class SpatialBrightnessEqualizer {
	public SpatialBrightnessEqualizer() {
		
	}
	
	/**
	 * Detects tiny change in pixel.
	 * @param mat
	 * @param spatialRange 
	 * @return
	 */
	public Matrix equalize(Matrix mat, int spatialRange){
		mat = new MatrixTypeConverter().convert(mat, Matrix.BLACK_WHITE);
		
		int rows = mat.getRows(),
			cols = mat.getCols();
		
		//create new mat//
		Matrix mat2 = new Matrix(rows, cols, mat.getType());
		
		double ratio;
		int mediumValue = 0;
		for(int x=spatialRange, y; x<rows-spatialRange; x++){
			for(y=spatialRange; y<cols-spatialRange; y++){
				
				ratio = 128 / MatrixUtils.getMeanPixelValue(mat
						.subMatrix(x-spatialRange, x+spatialRange, y-spatialRange, y+spatialRange));
				mediumValue = (int) ((mat.pixels[x][y][0]+1) * ratio);
				
				if(mediumValue>255){
					mediumValue=255;
				}
				
				mat2.pixels[x][y][0] = mediumValue;
			}
		}
		
		return mat2;
	}
}
