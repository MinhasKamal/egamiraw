/************************************************************************************************************
* Developer: Minhas Kamal(minhaskamal024@gmail.com)															*
* Date: Dec-2015																							*
************************************************************************************************************/

package com.minhaskamal.egami.matrix.enhancer;

import com.minhaskamal.egami.matrix.Matrix;
import com.minhaskamal.egami.matrix.converter.MatrixTypeConverter;
import com.minhaskamal.egami.matrix.utils.MatrixUtils;

public class ContrastStretcher {
	public ContrastStretcher() {
		
	}
	
	/**
	 * grey-scale image only
	 * @param mat
	 * @return
	 */
	public Matrix histogramEqualizer(Matrix mat){
		mat = new MatrixTypeConverter().convert(mat, Matrix.BLACK_WHITE);
		
		int rows = mat.getRows(),
			cols = mat.getCols();
		double totalPixelCount = rows*cols;
		int[] pixelFreq = MatrixUtils.countPixelFreq(mat)[0];
		double[] cumulativeDistributiveValues =  new double[256];
		
		cumulativeDistributiveValues[0] = (pixelFreq[0]/totalPixelCount)*255;
		for(int i=1; i<256; i++){
			cumulativeDistributiveValues[i] = cumulativeDistributiveValues[i-1] + (pixelFreq[i]/totalPixelCount)*255;
		}
		
		//create new mat//
		Matrix mat2 = new Matrix(rows, cols, mat.getType());
		
		int newPixel = 0;
		for(int x=0, y; x<rows; x++){
			for(y=0; y<cols; y++){
				newPixel = (int) cumulativeDistributiveValues[ mat.pixels[x][y][0] ];
				
				mat2.pixels[x][y][0] = newPixel;
			}
		}
		
		return mat2;
	}
	
	public Matrix biHistogramEqualizer(Matrix mat) {
		mat = new MatrixTypeConverter().convert(mat, Matrix.BLACK_WHITE);
		
		int rows = mat.getRows(),
			cols = mat.getCols();
		int totalPixelCount = rows*cols;
		int[] pixelFreq = MatrixUtils.countPixelFreq(mat)[0];

		///////////
		int mediumPixel = 0;
		double lowerMediumPixelCount = 0,
			higherMediumPixelCount = 0;
		for (int i=0; i<256; i++) {
			lowerMediumPixelCount += pixelFreq[i];
			if (lowerMediumPixelCount > totalPixelCount/2) {
				mediumPixel = i-1;
				break;
			}
		}
		if (mediumPixel<0) {
			mediumPixel = 0;
		} else {
			lowerMediumPixelCount -= pixelFreq[mediumPixel+1];
		}
		higherMediumPixelCount = totalPixelCount-lowerMediumPixelCount;

		/////////////
		double[] cumulativeDistributiveValues = new double[256];

		cumulativeDistributiveValues[0] = (pixelFreq[0]/lowerMediumPixelCount) * mediumPixel;
		for (int i=1; i<=mediumPixel; i++) {
			cumulativeDistributiveValues[i] = cumulativeDistributiveValues[i-1] +
					(pixelFreq[i]/lowerMediumPixelCount) * mediumPixel;
		}
		for (int i = mediumPixel + 1; i <= 255; i++) {
			cumulativeDistributiveValues[i] = cumulativeDistributiveValues[i-1] +
					(pixelFreq[i] / higherMediumPixelCount) * (255 - mediumPixel - 1);
		}

		////////////
		Matrix mat2 = new Matrix(rows, cols, mat.getType());

		int[] newPixel = new int[1];
		for (int x = 0, y; x < rows; x++) {
			for (y = 0; y < cols; y++) {
				newPixel[0] = (int) cumulativeDistributiveValues[mat.pixels[x][y][0]];

				mat2.pixels[x][y] = newPixel.clone();
			}
		}

		return mat2;
	}
	
	public Matrix spatialBiHistogramEqualizer(Matrix mat, int sectionWidth, int sectionHeight) {
		mat = new MatrixTypeConverter().convert(mat, Matrix.BLACK_WHITE);
		
		int rows = mat.getRows(),
			cols = mat.getCols();
		
		Matrix mat2 = new Matrix(rows, cols, Matrix.BLACK_WHITE);
		
		Matrix subMat;
		for(int i=0; i<rows-sectionHeight; i+=sectionHeight){
			for(int j=0; j<cols-sectionWidth; j+=sectionWidth){
				subMat = mat.subMatrix(i, i+sectionHeight, j, j+sectionWidth);
				System.out.println(i+"-"+j+" "+subMat.getCols());
				subMat = biHistogramEqualizer(subMat);
				
				for(int m=0; m<sectionHeight ; m++){
					for(int n=0; n<sectionWidth ; n++){
						mat2.pixels[m+i][n+j] = subMat.pixels[m][n];
					}
				}
			}
		}
		
		return mat2;
	}
	
}
