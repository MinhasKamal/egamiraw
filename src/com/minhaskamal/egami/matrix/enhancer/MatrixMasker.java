/************************************************************************************************************
* Developer: Minhas Kamal(minhaskamal024@gmail.com)															*
* Date: Dec-2015																							*
************************************************************************************************************/

package com.minhaskamal.egami.matrix.enhancer;

import com.minhaskamal.egami.matrix.Matrix;
import com.minhaskamal.egami.matrix.editor.MatrixBorderCreator;

public class MatrixMasker {
	public MatrixMasker() {
		
	}
	
	public Matrix applyMask(Matrix matrix, double[][] mask) {
		Matrix matrix2 = new Matrix(matrix.getRows(), matrix.getCols(), matrix.getType());
		
		int border = (mask.length-1)/2;
		matrix = new MatrixBorderCreator().createBorder(matrix, border);
		
		int rows = matrix.getRows(),
			cols = matrix.getCols();
		int[] newpixel = new int[matrix.getType()];

		for(int i=border; i<rows-border; i++) {
			for(int j=border; j<cols-border; j++) {

				for(int k=0; k<newpixel.length; k++) {
					newpixel[k] = 0;

					for(int m=0; m<mask.length; m++) {
						for(int n=0; n<mask[0].length; n++) {
							newpixel[k] += (mask[m][n] * matrix.pixels[i+m-border][j+n-border][k]);
						}
					}
					/*if(newpixel[k] > Matrix.WHITE_PIXEL){
						newpixel[k]=255;
					}else if(newpixel[k] < Matrix.BLACK_PIXEL){
						newpixel[k]=0;
					}*/
				}
				matrix2.pixels[i-border][j-border] =  newpixel.clone();
				
			}
		}

		return matrix2;
	}
	

	public Matrix applyBlurring(Matrix mat, int level){
		int dimention = level*2+1;
		
		/*double[][] mask = new double[dimention][dimention];
		for(int i=0; i<dimention; i++){
			for(int j=0; j<dimention; j++){
				mask[i][j] = 1.0/(dimention*dimention);
			}
		}*/
		
		return applyMask(mat, getGaussianFilter(dimention, 1));
	}
	

	public Matrix applyVerticalEdgeDetection(Matrix mat){
		double[][] mask = {
				{1},
				{0},
				{-1}
		};
		
		Matrix mat2 = applyMask(mat, mask);
		
		return mat2;
	}
	
	public Matrix applyHorizontalEdgeDetection(Matrix mat){
		double[][] mask = {
				{1, 0, -1}
		};
		
		Matrix mat2 = applyMask(mat, mask);
		
		return mat2;
	}
	
	
	public double[][] getGaussianFilter(int size, int sigma){
		double[][] mask = new double[size][size];
		
		double divider1 = 2*Math.PI*sigma*sigma;
		double divider2 = 2*sigma*sigma;
		double sum=0;
		
		int distanceX, distanceY;
		for(int i=0; i<mask.length; i++){	//calculate gaussian
			distanceY = i-(mask.length/2);
			for(int j=0; j<mask[i].length; j++){
				distanceX = j-(mask[i].length/2);
				
				mask[i][j] = Math.exp( -(distanceX*distanceX+distanceY*distanceY)/(divider2) ) / divider1;
				sum+=mask[i][j];
			}
		}
		
		for(int i=0; i<mask.length; i++){	//normalization
			for(int j=0; j<mask[i].length; j++){
				mask[i][j] /= sum;
			}
		}
		
		return mask;
	}
	
	/**
	 * 
	 * @param angle <code>angle</code>==0?horizontal / <code>angle</code>==90?vertical /
	 * <code>angle</code> rotates anti-clockwise
	 * @return
	 */
	public double[][] getSobelFilter(int angle){
		double[][] mask = new double[3][3];
		
		int[][] cycle = new int[][]{
			{0, 2},
			{0, 1},
			{0, 0},
			{1, 0},
			{2, 0},
			{2, 1},
			{2, 2},
			{1, 2}
		};
		
		int[] values = new int[]{
			-1, -2, -1, 0, 1, 2, 1, 0
		};
		
		angle = angle%360;
		int startingPoint = angle/45;

		
		for(int i=0, point=startingPoint; i<cycle.length; i++, point++){
			if(point>=cycle.length){
				point=0;
			}
			
			mask[cycle[point][0]][cycle[point][1]] = values[i];
		}
		
		return mask;
	}
	
	public double[][] getHighboostMask(int a){
		double[][] mask = new double[][]
				{{0, -1, 0},
		        {-1, 4+a, -1},
		        {0, -1, 0}};
		
		return mask;
	}
	
	public double[][] getLaplacianMask(int a){
		return getHighboostMask(0);
	}
}
