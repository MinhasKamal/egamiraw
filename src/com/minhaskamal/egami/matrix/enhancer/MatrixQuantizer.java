/************************************************************************************************************
* Developer: Minhas Kamal(minhaskamal024@gmail.com)															*
* Date: Dec-2015																							*
************************************************************************************************************/

package com.minhaskamal.egami.matrix.enhancer;

import com.minhaskamal.egami.matrix.Matrix;
import com.minhaskamal.egami.matrix.converter.MatrixTypeConverter;
import com.minhaskamal.egami.matrix.utils.MatrixUtils;

public class MatrixQuantizer {
	public MatrixQuantizer() {
		
	}
	
	/**
	 * Implements image quantization technique & resets pixels of a grey scale
	 * image to specific pixels. Works with only <b>grey scale</b> image.
	 * @param mat input <code>Matrix</code>.
	 * @param numberOfColors 
	 * @return converted <code>Matrix</code>
	 */
	public Matrix applyQuantization(Matrix mat, int numberOfColors){
		mat = new MatrixTypeConverter().convert(mat, Matrix.BLACK_WHITE);

		int rows = mat.getRows(),
			cols = mat.getCols();
		
		//create new mat//
		Matrix newMat = new Matrix(rows, cols, Matrix.BLACK_WHITE);
		
		int limitMin = 0,
			limitMax = 255,
			range = (limitMax-limitMin)/(numberOfColors-1);
		
		double mediumPixel = MatrixUtils.getMeanPixelValue(mat);
		
		int adaptiveLimitMin=limitMin,
			adaptiveLimitMax=limitMax;
		if(mediumPixel>128){
			adaptiveLimitMin = (int) (limitMin + 2*(mediumPixel-128));
		}else{
			adaptiveLimitMax = (int) (limitMax - 2*(128-mediumPixel));
		}
		
		int adaptiveRange = (adaptiveLimitMax-adaptiveLimitMin) / numberOfColors;
		
		int pixel=0;
		for(int x=0, y; x<rows; x++){
			for(y=0; y<cols; y++){
				
				pixel = (int) mat.pixels[x][y][0];
				for(int i=0; i<numberOfColors; i++){
					if( pixel < (adaptiveLimitMin+(adaptiveRange*(i+1))) ){
						pixel=range*i;
						break;
					}
				}
				if(pixel==(int) mat.pixels[x][y][0]){//TODO
					System.out.println(pixel + " " + numberOfColors);
				}
				newMat.pixels[x][y] = new int[]{pixel};
			}
		}
		
		return newMat;
	}
}
