/***********************************************************
* Developer: Minhas Kamal(minhaskamal024@gmail.com)        *
* Date: Dec-2015                                           *
***********************************************************/

package com.minhaskamal.egami.matrixMath;

public class MatrixMath {
	
	public MatrixMath() {
		
	}
	
	public int[][] addMatrixByMatrix(int[][] matrix1, int[][] matrix2){
		int[][] resultMatrix = new int [matrix1.length][matrix1[0].length];
		
		for(int i=0; i<resultMatrix.length; i++){
			for(int j=0; j<resultMatrix[0].length; j++){
				resultMatrix[i][j] = matrix1[i][j]+matrix2[i][j];
			}
		}
		
		return resultMatrix;
	}
	
	public int[] addArrayByArray(int[] array1, int[] array2){
		return addMatrixByMatrix(new int[][]{array1}, new int[][]{array2})[0];
	}
	
	public int[][] subtracMatrixByMatrix(int[][] matrix1, int[][] matrix2){
		int[][] resultMatrix = new int [matrix1.length][matrix1[0].length];
		
		for(int i=0; i<resultMatrix.length; i++){
			for(int j=0; j<resultMatrix[0].length; j++){
				resultMatrix[i][j] = matrix1[i][j]+matrix2[i][j];
			}
		}
		
		return resultMatrix;
	}
	
	public int[] subtracArrayByArray(int[] array1, int[] array2){
		return subtracMatrixByMatrix(new int[][]{array1}, new int[][]{array2})[0];
	}

	public double[][] divideMatrixByNumber(int[][] matrix1, double number){
		double[][] resultMatrix = new double[matrix1.length][matrix1[0].length];
		
		for(int i=0; i<resultMatrix.length; i++){
			for(int j=0; j<resultMatrix[0].length; j++){
				resultMatrix[i][j] = matrix1[i][j]/number;
			}
		}
		
		return resultMatrix;
	}
	
	public double[] divideArrayByNumber(int[] array, double number){
		return divideMatrixByNumber(new int[][]{array}, number)[0];
	}
	
	public int[][] multiplyMatrixByMatrixPointTpPoint(int[][] matrix1, int[][] matrix2){
		int[][] resultMatrix = new int [matrix1.length][matrix1[0].length];
		
		for(int i=0; i<resultMatrix.length; i++){
			for(int j=0; j<resultMatrix[0].length; j++){
				resultMatrix[i][j] = matrix1[i][j]*matrix2[i][j];
			}
		}
		
		return resultMatrix;
	}
	
//	public static void main(String[] args) {
//		double[] res = new MatrixMath().divideArrayByNumber(new int[]{1, 2, 3}, 2);
//		
//		for(int i=0; i<res.length; i++){
//			System.out.println(res[i]);
//		}
//	}
}
