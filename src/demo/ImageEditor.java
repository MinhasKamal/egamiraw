/***********************************************************
* Developer: Minhas Kamal(minhaskamal024@gmail.com)        *
* Date: 01-Feb-2016                                        *
* Website: https://github.com/MinhasKamal/Egami            *
* License:  GNU General Public License version-3           *
***********************************************************/

package demo;

import java.io.IOException;

import javax.swing.UIManager;

import com.minhaskamal.egami.matrix.*;
import com.minhaskamal.egami.matrixUtil.*;
import com.minhaskamal.egami.view.ImageViewer;

public class ImageEditor {
	public static void main(String[] args) throws IOException {
		///create matrix object
		Matrix matrix = new Matrix("src/res/imgs/squares.png", Matrix.RED_GREEN_BLUE_ALPHA);
//		Matrix matrix = new Matrix("src/res/imgs/squares.png", Matrix.BLACK_WHITE);
		
		///convert RGB image to Grayscale image
//		matrix = MatrixUtilitiesPrimary.convertTo(matrix, Matrix.BLACK_WHITE);
		
		///test load, dump & clone
//		Matrix newMatrix = Matrix.load(matrix.dump());
//		matrix = newMatrix.clone();
		
		///skew border
//		matrix = new MatrixUtilities().skewVertical(matrix, -30);
//		matrix = new MatrixUtilities().skewHorizontal(matrix, 30);
				
		///rotate
//		matrix = new MatrixUtilities().rotate(matrix, 20);
			
		///rotate method 2
		matrix = new MatrixUtilities().rotate2(matrix, 15);
		
		///resizing
		matrix = new MatrixUtilities().verticallyStretch(matrix, 200);
			
		///create border
		matrix = new MatrixUtilities().createBorder(matrix, 4);
				
				
		///masking
//		double[][] mask = {	//vertical sobel mask
//				{-1, 0, 1},
//				{-2, 0, 2},
//				{-1, 0, 1}
//		};
//		double[][] mask = {	//gaussian mask
//				{4.0/33, 4.0/33, 4.0/33},
//				{4.0/33, 1.0/33, 4.0/33},
//				{4.0/33, 4.0/33, 4.0/33}
//		};
//		matrix = MatrixUtilitiesSecondary.applyMask(matrix, mask);
		
		
		///show result in image viewer
		try {
			UIManager.setLookAndFeel("javax.swing.plaf.nimbus.NimbusLookAndFeel");
		} catch(Exception e){/*do nothing*/}
		ImageViewer.viewImage(Matrix.matrixToBufferedImage(matrix));
		
		///save image on hard disk
//		matrix.write(System.getenv("SystemDrive")+System.getenv("HOMEPATH")+"\\Desktop\\squares.png");
//		System.out.println("Image saved!!!");
	}
}
