/***********************************************************
* Developer: Minhas Kamal(minhaskamal024@gmail.com)        *
* Date: 02-Feb-2016                                        *
***********************************************************/

package demo;

import java.io.IOException;
import javax.swing.UIManager;
import com.minhaskamal.egami.matrix.*;
import com.minhaskamal.egami.matrixUtil.*;
import com.minhaskamal.egami.view.ImageViewer;

public class ImageEditor2 {
	public static void main(String[] args) throws IOException {
		///create matrix object
		Matrix matrix = new Matrix("src/res/imgs/color_rectangle.png", Matrix.BLACK_WHITE);		
		
		///image quantization
//		matrix = new MatrixUtilities().applyQuantization(matrix, 3);
		
		///image edge detection
		matrix = new MatrixUtilities().applyEdgeDetecton(matrix);
				
		///image to LBP
//		matrix = new MatrixUtilities().applyLocalBinaryPattern(matrix);
		
		///image histogram equalization
//		matrix = new MatrixUtilities().histogramEqualizer(matrix);
		
		///image apply blurring
//		matrix = new MatrixUtilities().applyBlurring(matrix, 3);
		
		///show result in image viewer
		try {
			UIManager.setLookAndFeel("javax.swing.plaf.nimbus.NimbusLookAndFeel");
		} catch(Exception e){/*do nothing*/}
		ImageViewer.viewImage(Matrix.matrixToBufferedImage(matrix));
		
		///save image on hard disk
//		matrix.write(System.getenv("SystemDrive")+System.getenv("HOMEPATH")+"\\Desktop\\color_rectangle.png");
//		System.out.println("Image saved!!!");
	}
}
