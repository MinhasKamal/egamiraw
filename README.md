# :snowman: Egami 

[![Gitter](https://badges.gitter.im/MinhasKamal/Egami.svg)](https://gitter.im/MinhasKamal/Egami?utm_source=badge&utm_medium=badge&utm_campaign=pr-badge)

#### A Light Weight Image Processing Library

[This image processing library](http://minhaskamal.github.io/Egami/) implements various base image processing algorithms, ranging from image enhancement and filtering to feature extraction. This library is specially suitable for small projects.

### How to Use?
1. After [downloading](https://github.com/MinhasKamal/Egami/archive/master.zip) unzip the project, and import it to your IDE.
2. Now, attach it to your project's build path as a required project.
3. You can easily use this library like this-

```
public static void main(String[] args) throws IOException {
    //create matrix object//
    Matrix matrix = new Matrix("src/res/imgs/real.png", Matrix.BLACK_WHITE); //image path, image reading format
    
    //blur the image//
    matrix = new MatrixUtilities().applyBlurring(matrix, 3); //input matrix, level of blurring
    
    //write image to disk//
    String outputImagePath = System.getenv("SystemDrive") + System.getenv("HOMEPATH") + "\\Desktop\\" + out.png;
    matrix.write(outputImagePath);
}
```

So, from this image-

<div align="center">
  <img alt="real.png" src="https://cloud.githubusercontent.com/assets/5456665/16388432/f5d714a8-3cba-11e6-84f3-132b3502ad26.png" height="150" width=auto />
</div>

We get this-

<div align="center">
  <img alt="blur" src="https://cloud.githubusercontent.com/assets/5456665/16388429/f56cf258-3cba-11e6-8e98-722d4f87373e.png" height="150" width=auto />
</div>


A more clear insight of using the library can be found in the [test](https://github.com/MinhasKamal/Egami/tree/master/src/test) and [demo](https://github.com/MinhasKamal/Egami/tree/master/src/demo) section of the project. Projects like- [Alphabet Recognizer](https://github.com/MinhasKamal/AlphabetRecognizer), [Skin Detector](https://github.com/MinhasKamal/SkinDetector), and [Deep Gender Recognizer](https://github.com/MinhasKamal/DeepGenderRecognizer) may serve as a great source of further understanding.

### Demonstration

There are many things, can be done by Egami-

  <div align="center">
    <img src="https://cloud.githubusercontent.com/assets/5456665/16388430/f57759c8-3cba-11e6-96e6-2561859d2235.png" height="120" width=auto title="Histogram Equalization"/>
    <img src="https://cloud.githubusercontent.com/assets/5456665/16388431/f5a5a58a-3cba-11e6-8efb-8e45b9a4641f.png" height="120" width=auto title="Local Binary Pattern"/>
    <img src="https://cloud.githubusercontent.com/assets/5456665/16388428/f53ec8e2-3cba-11e6-99c7-074139d6e38d.png" height="120" width=auto title="Quantization"/>
    <img src="https://cloud.githubusercontent.com/assets/5456665/16490226/84cae202-3efa-11e6-91d6-f3035eea2457.png" height="120" width=auto title="Rotated by 60 deg"/>
    <img src="https://cloud.githubusercontent.com/assets/5456665/16490227/84cd7e90-3efa-11e6-9335-e1c9fc0a32c3.png" height="120" width=auto title="Skew by 30 deg"/>
  </div>
  
Here are some color-space conversions-
  
  <div align="center">
    <img src="https://cloud.githubusercontent.com/assets/5456665/16493934/40edf506-3f09-11e6-971c-5579501b1856.jpg" height="100" width=auto title="Actual Image"/>
    <img src="https://cloud.githubusercontent.com/assets/5456665/16493940/410bf3d0-3f09-11e6-976a-66a48a5148a2.png" height="100" width=auto title="Y Transformation"/>
    <img src="https://cloud.githubusercontent.com/assets/5456665/16493939/4107a7a8-3f09-11e6-972b-3b51a6727cf1.png" height="100" width=auto title="Cb Transformation"/>
    <img src="https://cloud.githubusercontent.com/assets/5456665/16493937/40f7fed4-3f09-11e6-86f0-f492df1febef.png" height="100" width=auto title="Value Transformation"/>
    <img src="https://cloud.githubusercontent.com/assets/5456665/16493938/40f92048-3f09-11e6-970c-13ebdb582c1b.png" height="100" width=auto title="Saturation Transformation"/>
    <img src="https://cloud.githubusercontent.com/assets/5456665/16493936/40f4a8ce-3f09-11e6-8d28-df37884d9b6d.png" height="100" width=auto title="Lightness Transformation"/>
  </div>
 
Segmentation-
 
  <div align="center">
    <img src="https://cloud.githubusercontent.com/assets/5456665/18299435/a2a02df0-74e2-11e6-9dc1-dcfdc87906a3.jpg" height="130" width=auto title="Actual Image"/>
    <img src="https://cloud.githubusercontent.com/assets/5456665/18299437/a2a59f2e-74e2-11e6-9b5b-b6ce0174feee.png" height="130" width=auto title="K-Means Cluster"/>
    <img src="https://cloud.githubusercontent.com/assets/5456665/18299436/a2a50a32-74e2-11e6-9374-6a84ad9368cb.png" height="130" width=auto title="Mean Shift Cluster"/>
  </div>


### License
<a rel="license" href="http://www.gnu.org/licenses/gpl.html"><img alt="GNU General Public License" style="border-width:0" src="http://www.gnu.org/graphics/gplv3-127x51.png" /></a><br/>Egami is licensed under a <a rel="license" href="http://www.gnu.org/licenses/gpl.html">GNU General Public License version-3</a>.
